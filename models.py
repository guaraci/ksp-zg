import math

from django.contrib.auth.models import AbstractUser
from django.contrib.gis.db import models as gis_models
from django.contrib.gis.geos import MultiPolygon, Point, Polygon
from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
from django.utils.functional import classproperty

from colorful.fields import RGBColorField

import requests

from observation.model_utils import PlotBase, PlotObsBase

SLOPE_MAPPING = {}


class Inventory:
    def __init__(self, period, num_plots, municipality=None):
        self.period = period  # 1, 2, 3
        self.num_plots = num_plots
        self.municipality = municipality

    @property
    def year(self):
        return {1: '1972-74', 2: '1991-93', 3: '2008-10'}.get(self.period)

    @property
    def plotobs_url(self):
        return reverse('plotobs') + f'?period={self.period}&gem={self.municipality.pk}'


class User(AbstractUser):
    class Meta:
        db_table = 'auth_user'


class AdminRegion(models.Model):
    REGION_TYPES = (
        ('frw', 'Forstrevier'),
        ('erhw', 'Erholungswald'),
        ('schw', 'Schutzwald'),
        ('nat', 'Naturschutzgebiet'),
        ('warm', 'Wärmegliederung'),
        ('frei', 'Freiperimeter'),
    )
    nr = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=100)
    region_type = models.CharField(max_length=10, choices=REGION_TYPES)
    geom = gis_models.MultiPolygonField(srid=2056, null=True, blank=True)

    geom_field = 'geom'

    class Meta:
        db_table = 'adminregion'

    def __str__(self):
        return "%s %s (%s)" % (self.get_region_type_display(), self.name, self.nr)


class Municipality(models.Model):
    code = models.CharField(max_length=1)
    name = models.CharField(max_length=50, unique=True)
    bfs_nr = models.IntegerField(null=True)
    plz = models.CharField(max_length=4, blank=True)
    kanton = models.CharField(max_length=2)
    the_geom = gis_models.MultiPolygonField(srid=2056, blank=True, null=True)

    class Meta:
        db_table = 'municipality'

    def __str__(self):
        return self.name

    @property
    def name_with_k(self):
        return self.name if not self.kanton or self.kanton == 'ZG' else f'{self.name} ({self.kanton})'

    @classmethod
    def fill_from_geoadmin(cls):
        data_url = 'https://api3.geo.admin.ch/rest/services/api/MapServer/find'
        params = {
            'layer': 'ch.swisstopo.swissboundaries3d-gemeinde-flaeche.fill',
            'searchText': 'ZG',
            'searchField': 'kanton',
            'returnGeometry': 'true',
            'sr': '2056',
        }
        response = requests.get(data_url, params=params)
        data = response.json()
        for entry in data['results']:
            Municipality.objects.create(
                code=entry['attributes']['gemname'][0],
                name=entry['attributes']['gemname'],
                bfs_nr=entry['id'],
                kanton='ZG',
                the_geom=MultiPolygon([Polygon(ring, srid=2036) for ring in gem['geometry']['rings']]),
            )


class IncClass(models.Model):
    code = models.PositiveSmallIntegerField(unique=True)
    description = models.CharField("Beschreibung", max_length=20)

    class Meta:
        db_table = 'lt_incclass'
        verbose_name = "Ertragsklasse"
        verbose_name_plural = "Ertragsklassen"

    def __str__(self):
        return f"Ertragsklasse {self.code} ({self.description})"


class Phytosoc(models.Model):
    code = models.CharField(max_length=6, unique=True)
    description = models.CharField("Beschreibung", max_length=200)
    inc_class = models.ForeignKey(IncClass, on_delete=models.PROTECT, verbose_name="Ertragsklasse")

    class Meta:
        db_table = 'phytosociology'
        verbose_name = "Phytosoziologie"
        verbose_name_plural = "Phytosoziologie"

    def __str__(self):
        return self.code


class PlotAbsence(models.Model):
    description = models.CharField("Beschreibung", max_length=40)

    class Meta:
        db_table = 'lt_plot_absence'

    def __str__(self):
        return self.description


class Plot(PlotBase):
    RADIUS_MAPPING = {
        25: 10.1,
        30: 10.2,
        35: 10.3,
        40: 10.4,
        45: 10.5,
        50: 10.6,
        55: 10.7,
        60: 10.8,
        65: 10.9,
        70: 11.0,
        75: 11.2,
        80: 11.3,
        85: 11.4,
        90: 11.5,
        95: 11.7,
        100: 11.9,
        105: 12.0,
        110: 12.2,
        115: 12.3,
        120: 12.5,
        125: 12.6,
    }
    nr = models.CharField("KSP Nummer", max_length=15)
    center = gis_models.PointField("KSP Zentrum", srid=2056)
    slope = models.PositiveSmallIntegerField("Neigung", default=0)
    sealevel = models.PositiveSmallIntegerField("Höhe ü. Meer", blank=True, null=True)
    offset_dist = models.PositiveSmallIntegerField("Verschiebung Distanz", default=0)
    offset_azi = models.PositiveSmallIntegerField("Verschiebung Azimuth", default=0)
    mirror_dist = models.PositiveSmallIntegerField("Spiegelung Distanz", default=0)
    mirror_azi = models.PositiveSmallIntegerField("Spiegelung Azimuth", default=0)
    absent = models.ForeignKey(PlotAbsence, null=True, blank=True, on_delete=models.PROTECT, verbose_name="Fehlend")
    remarks = models.TextField("Bemerkungen", blank=True)

    geom_field = 'center'

    class Meta:
        db_table = 'plot'

    def __str__(self):
        return f'Plot nr.{self.nr}'

    @classproperty
    def slope_mapping(cls):
        return self.RADIUS_MAPPING

    @property
    def radius(self):
        return self.RADIUS_MAPPING.get(round(self.slope / 5) * 5, 10)

    @property
    def the_geom(self):
        return self.center

    @property
    def point_exact(self):
        return self.center


class PlotObsStatus(models.Model):
    code = models.PositiveSmallIntegerField()
    description = models.CharField("Beschreibung", max_length=30)

    class Meta:
        db_table = 'lt_plot_obs_status'

    def __str__(self):
        return self.description


class Mutation(models.Model):
    code = models.CharField(max_length=1, unique=True)
    short_descr = models.CharField("Kurzbeschreibung", max_length=30)
    long_descr = models.CharField("Langbeschreibung", max_length=100)

    class Meta:
        db_table = 'lt_mutation'

    def __str__(self):
        return self.short_descr


class DevelStage(models.Model):
    code = models.PositiveSmallIntegerField()
    description = models.CharField("Beschreibung", max_length=80)

    class Meta:
        db_table = 'lt_devel_stage'

    def __str__(self):
        return self.description


class ForestMixture(models.Model):
    code = models.PositiveSmallIntegerField()
    description = models.CharField("Beschreibung", max_length=80)

    class Meta:
        db_table = 'lt_forest_mixture'

    def __str__(self):
        return self.description


class CrownClosure(models.Model):
    code = models.PositiveSmallIntegerField()
    description = models.CharField("Beschreibung", max_length=80)

    class Meta:
        db_table = 'lt_crown_closure'

    def __str__(self):
        return self.description


class OwnerCategory(models.Model):
    description = models.CharField("Kategorie", max_length=20)

    class Meta:
        db_table = 'lt_owner_category'

    def __str__(self):
        return self.description


class OwnerType(models.Model):
    code = models.CharField(max_length=1, unique=True)
    description = models.CharField("Beschreibung", max_length=30)
    categ = models.ForeignKey(OwnerCategory, on_delete=models.PROTECT, verbose_name="Kategorie")

    class Meta:
        db_table = 'ownertype'

    def __str__(self):
        return self.description


class Owner(models.Model):
    code = models.CharField(max_length=1)
    id_spa = models.PositiveSmallIntegerField(blank=True, null=True)
    name = models.CharField("Name", max_length=80)
    typ = models.ForeignKey(OwnerType, on_delete=models.PROTECT, db_column="type_id", verbose_name="Eigentümertyp")
    geom = gis_models.MultiPolygonField(srid=2056, blank=True, null=True)

    class Meta:
        db_table = 'owner'

    def __str__(self):
        return self.name


class Division(models.Model):
    code = models.CharField(max_length=1, unique=True)
    description = models.CharField("Beschreibung", max_length=30)

    class Meta:
        db_table = 'division'

    def __str__(self):
        return self.description


class HerbLayer(models.Model):
    code = models.PositiveSmallIntegerField()
    description = models.CharField("Beschreibung", max_length=80)

    class Meta:
        db_table = 'lt_herblayer'

    def __str__(self):
        return self.description


class Landslide(models.Model):
    code = models.PositiveSmallIntegerField()
    description = models.CharField("Beschreibung", max_length=80)

    class Meta:
        db_table = 'lt_landslide'

    def __str__(self):
        return self.description


class MecDamage(models.Model):
    code = models.PositiveSmallIntegerField()
    description = models.CharField("Beschreibung", max_length=80)

    class Meta:
        db_table = 'lt_mecdamage'

    def __str__(self):
        return self.description


class PlotObs(PlotObsBase):
    PERIOD_CHOICES = (
        (1, 'Inventur 1 (1972-1974)'),
        (2, 'Inventur 2 (1991-1993)'),
        (3, 'Inventur 3 (2008-2010)'),
        (4, 'Inventur 4 (2022?-?)'),
    )
    plot = models.ForeignKey(Plot, verbose_name="Aufnahmepunkt (plot)", on_delete=models.CASCADE)
    inv_period = models.PositiveSmallIntegerField("Aufnahmeperiode", choices=PERIOD_CHOICES)
    inv_spezial = models.BooleanField("Spezialinventur", default=False)
    year = models.PositiveSmallIntegerField("Aufnahmejahr", db_index=True)
    quarter = models.PositiveSmallIntegerField("Aufnahmequartal", blank=True, null=True)
    status = models.ForeignKey(PlotObsStatus, on_delete=models.PROTECT, verbose_name="Status")
    remarks_status = models.TextField("Bemerkungen zum Status", blank=True)
    mutation = models.ForeignKey(Mutation, null=True, blank=True, on_delete=models.PROTECT, verbose_name="Mutation")
    devel_stage = models.ForeignKey(DevelStage, null=True, on_delete=models.PROTECT, verbose_name="Entwicklungstufe")
    forest_mixture = models.ForeignKey(ForestMixture, null=True, on_delete=models.PROTECT, verbose_name="Mischungsgrad")
    crown_closure = models.ForeignKey(CrownClosure, null=True, on_delete=models.PROTECT, verbose_name="Schlussgrad")
    devel_stage_map = models.ForeignKey(
        DevelStage, null=True, on_delete=models.PROTECT, verbose_name="Entwicklungstufe (Karte)", related_name='+'
    )
    forest_mixture_map = models.ForeignKey(
        ForestMixture, null=True, on_delete=models.PROTECT, verbose_name="Mischungsgrad (Karte)", related_name='+'
    )
    crown_closure_map = models.ForeignKey(
        CrownClosure, null=True, on_delete=models.PROTECT, verbose_name="Schlussgrad (Karte)", related_name='+'
    )
    survey = models.CharField("Erhebung", blank=True, max_length=5)
    municipality = models.ForeignKey(Municipality, blank=True, null=True, on_delete=models.PROTECT, verbose_name="Gemeinde")
    owner = models.ForeignKey(Owner, blank=True, null=True, on_delete=models.PROTECT, verbose_name="Eigentümer")
    division = models.ForeignKey(Division, blank=True, null=True, on_delete=models.PROTECT, verbose_name="Abteilung")
    remarks_ecode = models.TextField("Bemerkungen zum E-CODE", blank=True)
    stand_age = models.PositiveSmallIntegerField("Bestandalter", blank=True, null=True)
    herb_layer = models.ForeignKey(HerbLayer, blank=True, null=True, on_delete=models.PROTECT, verbose_name="Krautschicht")
    landslide = models.ForeignKey(Landslide, blank=True, null=True, on_delete=models.PROTECT, verbose_name="Rutschung")
    dead_wood = models.PositiveSmallIntegerField("Totholz", blank=True, null=True)
    mec_damage = models.ForeignKey(MecDamage, blank=True, null=True, on_delete=models.PROTECT, verbose_name="Mechanische Schäden")
    soil_cond = models.PositiveSmallIntegerField("Ungünstige Bodenverhältnisse", blank=True, null=True)
    remarks = models.TextField("Allgemeine Bemerkungen", blank=True)

    class Meta:
        db_table = 'plot_obs'
        constraints = [
            models.CheckConstraint(check=models.Q(year__lte=2100, year__gte=1970), name='year_range_check'),
            models.CheckConstraint(check=models.Q(quarter__lte=4), name='quarter_max_4'),
        ]

    def __str__(self):
        return f'Plot {self.plot.nr}: obs {self.year}'

    def visible_fields(self, exclude_empty=True):
        """
        Return a list of interesting/visible field names for the plot, having a
        non-empty value.
        """
        return [
            field.name
            for field in self._meta.fields
            if (not exclude_empty or getattr(self, field.name) not in (None, ''))
        ]


class MarkerType(models.Model):
    code = models.CharField(max_length=1)
    description = models.CharField(max_length=50)

    class Meta:
        db_table = 'marker_type'


class Marker(models.Model):
    plot = models.ForeignKey(Plot, on_delete=models.CASCADE)
    nr = models.CharField("Nummer", max_length=15)
    mtype = models.ForeignKey(MarkerType, blank=True, null=True, on_delete=models.PROTECT, verbose_name='Typ')
    azimuth = models.PositiveSmallIntegerField("GON (400)")
    distance = models.PositiveSmallIntegerField("in Dezimetern [dm]")

    class Meta:
        db_table = 'marker'


class TreeGroup(models.Model):
    name = models.CharField(max_length=25, unique=True)

    class Meta:
        db_table = 'tree_group'

    def __str__(self):
        return self.name


class TreeSpecies(models.Model):
    TARIFKLASS_CHOICES = (
        ('fichte', 'Fichte (HAHBART1)'),
        ('tanne', 'Tanne (HAHBART2)'),
        ('buche', 'Buche (HAHBART7)'),
        ('ubrige_laub', 'übrige Laubhölzer (HAHBART12)'),
        ('ubrige_nad', 'übrige Nadelhölzer (HAHBART6)'),
    )
    code = models.CharField(max_length=4)
    description = models.CharField(max_length=100)
    group = models.ForeignKey(TreeGroup, null=True, blank=True, on_delete=models.SET_NULL)
    tarif_klass20 = models.CharField(max_length=15, choices=TARIFKLASS_CHOICES, blank=True)
    historic = models.BooleanField(default=False)
    color = RGBColorField("Farbe", blank=True)

    name_field = 'description'

    class Meta:
        db_table = 'lt_tree_spec'
        constraints = [
            models.UniqueConstraint(fields=['code', 'historic'], name='unique_code_non_historic')
        ]

    def __str__(self):
        return self.description


class Tree(models.Model):
    plot = models.ForeignKey(Plot, on_delete=models.CASCADE)
    nr = models.PositiveIntegerField("Baumnummer")
    old_nr = models.CharField("alte Nummer", max_length=15)
    spec = models.ForeignKey(TreeSpecies, blank=True, null=True, on_delete=models.PROTECT)
    azimuth = models.PositiveSmallIntegerField("GON (400)")
    distance = models.PositiveSmallIntegerField("in Dezimetern [dm]")
    mirror = models.BooleanField("Spiegelbaum?", default=False)

    class Meta:
        db_table = 'tree'

    def __str__(self):
        return f'Baum nr.{self.nr} (az:{self.azimuth} dist:{self.distance})'

    @property
    def point(self):
        """
        Calculate and return point from plot origin and relative distance/azimuth.
        """
        grad_to_rad = lambda val: val*math.pi/200
        plot_center = self.plot.point_exact or self.plot.the_geom
        return Point(
            plot_center.x + float(self.distance/10) * math.sin(grad_to_rad(self.azimuth)),
            plot_center.y + float(self.distance/10) * math.cos(grad_to_rad(self.azimuth)),
            srid=plot_center.srid
        )


class Vita(models.Model):
    code = models.CharField(max_length=1, unique=True)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'lt_vita'

    def __str__(self):
        return self.description


class TreeObs(models.Model):
    obs = models.ForeignKey(PlotObs, on_delete=models.CASCADE)
    tree = models.ForeignKey(Tree, on_delete=models.CASCADE, verbose_name="Baum")
    dbh = models.PositiveSmallIntegerField("Brusthöhendurchmesser in cm")
    vita = models.ForeignKey(Vita, on_delete=models.PROTECT, verbose_name="Lebenslauf")
    remarks = models.TextField("Bemerkungen", blank=True)

    class Meta:
        db_table = 'tree_obs'
        constraints = [
            models.UniqueConstraint(fields=['obs', 'tree'], name='unique_tree_per_plot_obs')
        ]

    def clean(self):
        if self.dbh == 0 and self.vita.code == 'L':
            raise ValidationError(f"Living trees should not have a diameter of 0 (plot: {self.obs.plot.nr})")


class TreeObsExtra(models.Model):
    """Extra data (D7/Höhe) that was taken during 1990 inventory."""
    tree_obs = models.OneToOneField(TreeObs, on_delete=models.CASCADE, related_name='+')
    d7 = models.PositiveSmallIntegerField("D7M")
    hoehe = models.PositiveSmallIntegerField("Höhe")

    class Meta:
        db_table = 'tree_obs_extra'


class ForestSocio(models.Model):
    phytosoc = models.ForeignKey(Phytosoc, on_delete=models.PROTECT, null=True, blank=True)
    kartierer = models.CharField("Kartierer", max_length=50)
    geom = gis_models.PolygonField("Geometrie", srid=2056)
    remarks = models.TextField("Bemerkungen", blank=True)

    class Meta:
        db_table = 'waldgesellschaft'
        verbose_name = "Waldgesellschaft"
        verbose_name_plural = "Waldgesellschaften"


class WaldBestandKarte(models.Model):
    entwicklungstufe = models.ForeignKey(DevelStage, blank=True, null=True,
        db_column='es', on_delete=models.SET_NULL)
    mischungsgrad = models.ForeignKey(ForestMixture, blank=True, null=True,
        db_column='mg', on_delete=models.SET_NULL)
    schlussgrad = models.ForeignKey(CrownClosure, blank=True, null=True,
        db_column='sg', on_delete=models.SET_NULL)
    remarks = models.TextField("Bemerkungen", blank=True)
    geom = gis_models.PolygonField(srid=2056)

    class Meta:
        db_table = 'waldbestandkarte'
