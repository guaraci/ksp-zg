from django.contrib import admin
from django.contrib.gis.admin import GISModelAdmin

from . import models


@admin.register(models.AdminRegion)
class AdminRegionAdmin(GISModelAdmin):
    list_display = ['nr', 'name', 'region_type']
    list_filter = ['region_type']


@admin.register(models.Municipality)
class MunicipalityAdmin(admin.ModelAdmin):
    list_display = ['name', 'plz', 'bfs_nr', 'code']


@admin.register(models.IncClass)
class IncClassAdmin(admin.ModelAdmin):
    list_display = ['description', 'code']


@admin.register(models.Phytosoc)
class PhytosocAdmin(admin.ModelAdmin):
    list_display = ['description', 'code', 'inc_class']


@admin.register(models.PlotAbsence)
class PlotAbsenceAdmin(admin.ModelAdmin):
    list_display = ['description']


@admin.register(models.Plot)
class PlotAdmin(admin.ModelAdmin):
    list_display = ['nr', 'sealevel']


@admin.register(models.PlotObsStatus)
class PlotObsStatusAdmin(admin.ModelAdmin):
    list_display = ['description', 'code']


@admin.register(models.Mutation)
class MutationAdmin(admin.ModelAdmin):
    list_display = ['short_descr', 'code', 'long_descr']


@admin.register(models.DevelStage)
class DevelStageAdmin(admin.ModelAdmin):
    list_display = ['description', 'code']


@admin.register(models.ForestMixture)
class ForestMixtureAdmin(admin.ModelAdmin):
    list_display = ['description', 'code']


@admin.register(models.CrownClosure)
class CrownClosureAdmin(admin.ModelAdmin):
    list_display = ['description', 'code']


@admin.register(models.OwnerCategory)
class OwnerCategoryAdmin(admin.ModelAdmin):
    list_display = ['description']


@admin.register(models.OwnerType)
class OwnerTypeAdmin(admin.ModelAdmin):
    list_display = ['description', 'code', 'categ']


@admin.register(models.Owner)
class OwnerAdmin(GISModelAdmin):
    list_display = ['name', 'code', 'typ', 'id_spa']


@admin.register(models.Division)
class DivisionAdmin(admin.ModelAdmin):
    list_display = ['description', 'code']


@admin.register(models.HerbLayer)
class HerbLayerAdmin(admin.ModelAdmin):
    list_display = ['description', 'code']


@admin.register(models.Landslide)
class LandslideAdmin(admin.ModelAdmin):
    list_display = ['description', 'code']


@admin.register(models.MecDamage)
class MecDamageAdmin(admin.ModelAdmin):
    list_display = ['description', 'code']


@admin.register(models.PlotObs)
class PlotObsAdmin(admin.ModelAdmin):
    list_display = ['year', 'quarter', 'status', ]


@admin.register(models.MarkerType)
class MarkerTypeAdmin(admin.ModelAdmin):
    list_display = ['description', 'code']


@admin.register(models.Marker)
class MarkerAdmin(admin.ModelAdmin):
    list_display = ['nr', 'mtype', 'azimuth', 'distance']


@admin.register(models.TreeGroup)
class TreeGroupAdmin(admin.ModelAdmin):
    list_display = ['name', 'species']

    def get_queryset(self, request):
        return super().get_queryset(request).prefetch_related('treespecies_set')

    def species(self, obj):
        return ",".join([sp.description for sp in obj.treespecies_set.all()])


@admin.register(models.TreeSpecies)
class TreeSpeciesAdmin(admin.ModelAdmin):
    list_display = ['description', 'code', 'historic']


@admin.register(models.Tree)
class TreeAdmin(admin.ModelAdmin):
    list_display = ['nr', 'spec', 'azimuth', 'distance', 'mirror']


@admin.register(models.Vita)
class VitaAdmin(admin.ModelAdmin):
    list_display = ['description', 'code']


@admin.register(models.TreeObs)
class TreeObsAdmin(admin.ModelAdmin):
    list_display = ['dbh', 'vita', 'remarks']
    raw_id_fields = ['obs', 'tree']


@admin.register(models.TreeObsExtra)
class TreeObsExtraAdmin(admin.ModelAdmin):
    list_display = ['d7', 'hoehe']
    raw_id_fields = ['tree_obs']


@admin.register(models.ForestSocio)
class ForestSocioAdmin(admin.ModelAdmin):
    list_display = ['phytosoc', 'kartierer', 'remarks']


@admin.register(models.WaldBestandKarte)
class WaldBestandKarteAdmin(admin.ModelAdmin):
    list_display = ['entwicklungstufe', 'mischungsgrad', 'schlussgrad', 'remarks']
