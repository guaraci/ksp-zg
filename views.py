from django.db.models import Count
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView

from observation.views import GemeindenView, PlotObsDataView as PlotObsDataViewBase
from .db_views import HomepageView
from .models import Inventory, Municipality, PlotObs


class ZGGemeindenView(GemeindenView):
    db_view = HomepageView
    has_waldflaeche_col = False


class GemeindeDetailView(TemplateView):
    template_name = 'gemeinde_detail.html'

    def get_context_data(self, **context):
        context = super().get_context_data(**context)
        gemeinde = get_object_or_404(Municipality, pk=self.kwargs['pk'])
        periods = PlotObs.objects.filter(municipality=gemeinde).values('inv_period'
            ).annotate(num_obs=Count('id')).order_by('inv_period')
        passed_inventories = [
            Inventory(line['inv_period'], line['num_obs'], gemeinde) for line in periods
        ]
        context.update({
            'gemeinde': gemeinde,
            'center': gemeinde.the_geom.centroid if gemeinde.the_geom else None,
            'passed_inventories': passed_inventories,
            'current_inventories': [],
            'regions': [],
            'can_add': False,
        })
        return context


class PlotObsDataView(PlotObsDataViewBase):
    def get_queryset(self):
        return PlotObs.objects.select_related('plot').filter(
            inv_period=self.request.GET.get('period'), municipality_id=self.request.GET.get('gem')
        )
