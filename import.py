import argparse
import os
import subprocess

from access_parser import AccessParser
import django
from django.conf import settings
from django.contrib.gis.geos import Point
from django.core.exceptions import ValidationError
from django.core.management import call_command
from django.db import transaction

access_tables = {
    'kopfdaten': '1972 bis 2010 KOPFDATEN (Kt_ZUG) (Wald)',
    'baumdaten': 'BAUMDATEN Kt_Zug 1972 bis 2010 (Wald)',
    # The following are not used:
    'kopf90': '1990_92 Kopfdaten (inkl 1970_74)',
    'kopf93': '1993 Kopfdaten (STURM)',
    'kopf08': '2008_10 Kopfdaten (inkl 1990_92)',
    'baum90': 'Baumdaten 1990_92 (inkl 1972_74) (Wald)',
    'baum93': 'Baumdaten 1993 (Sturm92) (Wald)',
    'baum08': 'Baumdaten 2008_10 (inkl 1990_92) (Wald)',
}


def line_from_table(db, table_name):
    table = db.parse_table(table_name)
    columns = list(table.keys())
    idx = 0
    try:
        while True:
            line = {}
            for col in columns:
                line[col] = table[col][idx]
            yield line
            idx += 1
    except IndexError:
        return


def to_int(value, default=0):
    if value == '':
        return default
    int_val = int(value)
    if value != int_val:
        raise ValueError(f"Converting {value} to integer is truncating the value")
    return int_val


class PlotImporter:
    def __init__(self, db, only_missing=False):
        self.db = db
        self.only_missing=only_missing
        self.status_dict = {st.code: st for st in PlotObsStatus.objects.all()}
        self.devel_dict = {dev.code: dev for dev in DevelStage.objects.all()}
        self.mixture_dict = {mix.code: mix for mix in ForestMixture.objects.all()}
        self.closure_dict = {clo.code: clo for clo in CrownClosure.objects.all()}
        self.municip_dict = {mun.code: mun for mun in Municipality.objects.all()}
        self.otype_dict = {tp.code: tp for tp in OwnerType.objects.all()}
        self.owner_dict = {(own.typ.code, own.code): own for own in Owner.objects.all()}
        self.division_dict = {div.code: div for div in Division.objects.all()}
        self.herb_dict = {herb.code: herb for herb in HerbLayer.objects.all()}
        self.herb_dict[''] = None
        self.land_dict = {land.code: land for land in Landslide.objects.all()}
        self.land_dict[''] = None
        self.mec_dict = {mec.code: mec for mec in MecDamage.objects.all()}
        self.mec_dict[''] = None

    def devel_from_code(self, code):
        # '600' from '631'
        code_int = to_int(code) // 100 * 100
        if not code_int in self.devel_dict:
            raise KeyError(f"No devel for code {code_int}")
        return self.devel_dict[code_int]

    def mixture_from_code(self, code):
        # '30' from '631'
        code_int = to_int(code) % 100 // 10 * 10
        if not code_int in self.mixture_dict:
            raise KeyError(f"No mixture for code {code_int}")
        return self.mixture_dict[code_int]

    def closure_from_code(self, code):
        # '1' from '631'
        code_int = to_int(code) % 10
        if not code_int in self.closure_dict:
            raise KeyError(f"No closure for code {code_int}")
        return self.closure_dict[code_int]

    def municip_from_code(self, code):
        assert len(code) == 4
        return self.municip_dict[code[0]]

    def otype_from_code(self, code):
        assert len(code) == 4
        return self.otype_dict[code[1]]

    def owner_from_code_and_type(self, code):
        assert len(code) == 4
        otype = self.otype_dict[code[1]]
        if not (code[1], code[2]) in self.owner_dict:
            raise KeyError(f"No owner for code {code[2]} and type {code[1]}/{otype.description}")
        return self.owner_dict[(code[1], code[2])]

    def division_from_code(self, code):
        assert len(code) == 4
        if not code[3] in self.division_dict:
            raise KeyError(f"No division for code {code[3]}")
        return self.division_dict[code[3]]

    def import_lines(self):
        errs = set()
        for line in line_from_table(self.db, access_tables['kopfdaten']):
            try:
                with transaction.atomic():
                    self.import_line(line)
            except KeyError as err:
                if len(str(err)) < 5:
                    # Stop importing to examine which is the too short error
                    breakpoint()
                errs.add(str(err))
        if errs:
            print(errs)

    def import_line(self, line):
        plot_key = str(int(line['KEY']))
        if self.only_missing and Plot.objects.filter(nr=plot_key).exists():
            return
        if line['IDX'] > 1000000:
            # Special case, see https://gitlab.com/guaraci/ksp-zg/-/issues/11
            return
        plot = Plot(
            nr=plot_key,
            center=Point(x=line['IDX'], y=line['IDY'], srid=21781),
            slope=to_int(line['NEIGUNG']),
            sealevel=to_int(line['HOEHE üM']),
            offset_dist=to_int(line['VERZ_DIST']),
            offset_azi=to_int(line['VERZ_AZI']),
            mirror_dist=to_int(line['SPGL_DIST']),
            mirror_azi=to_int(line['SPGL_AZI']),
            remarks=line['BEM_PLOT']
        )
        plot.center.transform(2056)
        # conversion from 21781 to 2056 may give some very small submeter precision, which is unneeded here.
        # https://gitlab.com/guaraci/ksp-zg/-/issues/16
        plot.center.x = int(plot.center.x)
        plot.center.y = int(plot.center.y)
        plot.full_clean()
        plot.save()

        # Handle some exceptions
        if plot_key in ['820223', '824235']:
            plot.absent = PlotAbsence.objects.get(description="Wald unzugänglich")
            plot.remarks = line['Allgemeine Bemerkungen']
            plot.save()
            return  # No PlotObs at all for those.
        elif plot_key in ['847265']:
            plot.absent = PlotAbsence.objects.get(description="Aufnahme fehlt")
            plot.remarks = line['Allgemeine Bemerkungen']
            plot.save()

        # Creating PlotObs instances for each inventory
        objs = []
        if line['(72/74) AUFN_JAHR'] != '':
            obs72 = PlotObs(
                plot=plot,
                year=line['(72/74) AUFN_JAHR'],
                quarter=line['(72/74) AUFN_QUART'],
                status=self.status_dict[line['(72/74) STATUS']],
                remarks_status=line['(72/74) BEM_STATUS'],
                devel_stage=self.devel_from_code(line['(72/74) BESTYP-TERR']),
                forest_mixture=self.mixture_from_code(line['(72/74) BESTYP-TERR']),
                crown_closure=self.closure_from_code(line['(72/74) BESTYP-TERR']),
                devel_stage_map=self.devel_from_code(line['(1971) BESTYP-KART']),
                forest_mixture_map=self.mixture_from_code(line['(1971) BESTYP-KART']),
                crown_closure_map=self.closure_from_code(line['(1971) BESTYP-KART']),
            )
            if line['(90/92) E-CODE']:
                # E-CODE, take the value from 90/92 (https://gitlab.com/guaraci/ksp-zg/-/issues/28)
                obs72.municipality = self.municip_from_code(line['(90/92) E-CODE'])
                obs72.owner = self.owner_from_code_and_type(line['(90/92) E-CODE'])
                obs72.division = self.division_from_code(line['(90/92) E-CODE'])
            obs72.full_clean()
            objs.append(obs72)

        if line['(90/92) AUFN_JAHR'] and plot_key in ['841295']:
            # Handle some exceptions
            plot.absent = PlotAbsence.objects.get(description="Rodung")
            plot.remarks = line['Allgemeine Bemerkungen']
            plot.save()
        elif line['(90/92) AUFN_JAHR'] != '':
            obs90 = PlotObs(
                plot=plot,
                year=line['(90/92) AUFN_JAHR'],
                quarter=line['(90/92) AUFN_QUART'],
                status=self.status_dict[line['(90/92) STATUS']],
                remarks_status=line['(90/92) BEM_STATUS'],
                # BESTYP-TERR:
                devel_stage=self.devel_from_code(line['(90/92) BESTYP-TERR']),
                forest_mixture=self.mixture_from_code(line['(90/92) BESTYP-TERR']),
                crown_closure=self.closure_from_code(line['(90/92) BESTYP-TERR']),
                devel_stage_map=self.devel_from_code(line['(1990) BESTYP-KART']),
                forest_mixture_map=self.mixture_from_code(line['(1990) BESTYP-KART']),
                crown_closure_map=self.closure_from_code(line['(1990) BESTYP-KART']),
                survey=line['(90/92) ERHEBUNG'],
                # E-CODE:
                municipality=self.municip_from_code(line['(90/92) E-CODE']),
                owner=self.owner_from_code_and_type(line['(90/92) E-CODE']),
                division=self.division_from_code(line['(90/92) E-CODE']),

                stand_age=to_int(line['(90/92) BEALTER']),
                herb_layer=self.herb_dict[line['(90/92) KRAUTSCH']],
                landslide=self.land_dict[line['(90/92) RUTSCHUNG']],
                dead_wood=line['(90/92) TOT_STAE'] if line['(90/92) TOT_STAE'] != '' else None,
                mec_damage=self.mec_dict[line['(90/92) MECH_SCHD']],
                soil_cond=line['(90/92) UNG_BODVER'] if line['(90/92) UNG_BODVER'] != '' else None,
            )
            obs90.full_clean()
            objs.append(obs90)

        if line['(93) AUFN_JAHR']:
            obs93 = PlotObs(
                plot=plot,
                year=1993,
                quarter=line['(93) AUFN_QUART'],
                status=self.status_dict[line['(93) STATUS']],
                devel_stage=self.devel_from_code(line['(93) BESTYP-TERR']),
                forest_mixture=self.mixture_from_code(line['(93) BESTYP-TERR']),
                crown_closure=self.closure_from_code(line['(93) BESTYP-TERR']),
                devel_stage_map=self.devel_from_code(line['(1990) BESTYP-KART']),
                forest_mixture_map=self.mixture_from_code(line['(1990) BESTYP-KART']),
                crown_closure_map=self.closure_from_code(line['(1990) BESTYP-KART']),
                # E-CODE, always identical to 90/92 code:
                municipality=self.municip_from_code(line['(90/92) E-CODE']),
                owner=self.owner_from_code_and_type(line['(90/92) E-CODE']),
                division=self.division_from_code(line['(90/92) E-CODE']),
            )
            obs93.full_clean()
            objs.append(obs93)

        if line['(2008/10) AUFN_JAHR'] and plot_key in [
            '823234', '827173', '828217', '842304', '850251', '856264', '883190', '904230', '913192'
        ]:
            # Handle some exceptions
            if plot_key == '883190':
                plot.absent = PlotAbsence.objects.get(description="Wald unzugänglich")
            else:
                plot.absent = PlotAbsence.objects.get(description="Aufnahme fehlt")
            plot.remarks = line['Allgemeine Bemerkungen']
            plot.save()
        elif to_int(line['(2008/10) STATUS']) == 3:
            remarks = line['(2008/10) BEM_STATUS']
            if 'rodung' in remarks.lower():
                plot.absent = PlotAbsence.objects.get(description='Rodung')
            if remarks not in plot.remarks:
                plot.remarks = remarks
            plot.save()
        elif line['(2008/10) AUFN_JAHR']:
            obs08 = PlotObs(
                plot=plot,
                year=line['(2008/10) AUFN_JAHR'],
                quarter=line['(2008/10) AUFN_QUART'],
                status=self.status_dict[line['(2008/10) STATUS']],
                remarks_status=line['(2008/10) BEM_STATUS'],
                survey=line['DB 08/10 ERHEBUNG'],
                devel_stage=self.devel_from_code(line['(2008/10) BESTYP-TERR']),
                forest_mixture=self.mixture_from_code(line['(2008/10) BESTYP-TERR']),
                crown_closure=self.closure_from_code(line['(2008/10) BESTYP-TERR']),
                devel_stage_map=self.devel_from_code(line['(2006) BESTYP-KART']),
                forest_mixture_map=self.mixture_from_code(line['(2006) BESTYP-KART']),
                crown_closure_map=self.closure_from_code(line['(2006) BESTYP-KART']),
                # E-CODE:
                municipality=self.municip_from_code(line['(2008/10) E-CODE']),
                owner=self.owner_from_code_and_type(line['(2008/10) E-CODE']),
                division=self.division_from_code(line['(2008/10) E-CODE']),
                remarks_ecode=line['(2008/10) BEM_E-CODE'],

                stand_age=to_int(line['(2008/10) BEALTER']),
                herb_layer=self.herb_dict[line['(2008/10) KRAUTSCH']],
                landslide=self.land_dict[line['(2008/10) RUTSCHUNG']],
                dead_wood=line['(2008/10) TOT_STAE'] if line['(2008/10) TOT_STAE'] != '' else None,
                mec_damage=self.mec_dict[line['(2008/10) MECH_SCHD']],
                soil_cond=line['(2008/10) UNG_BODVER'] if line['(2008/10) UNG_BODVER'] != '' else None,
                remarks=line['Allgemeine Bemerkungen'],
            )
            obs08.full_clean()
            objs.append(obs08)

        PlotObs.objects.bulk_create(objs)

    def import_mutations(self):
        """Import mutations specific to the 08/10 inventory."""
        mutation_dict = {mut.code: mut for mut in Mutation.objects.all()}
        for line in line_from_table(self.db, access_tables['kopf08']):
            if not line['MUTATION'] or len(line['MUTATION']) > 1:
                continue
            key = str(int(line['KEY']))
            try:
                obs = PlotObs.objects.get(plot__nr=key, year__gte=2008)
            except PlotObs.DoesNotExist:
                continue
            obs.mutation = mutation_dict[line['MUTATION']]
            obs.save()


class TreeImporter:
    def __init__(self, db):
        self.db = db
        self.species_dict = {sp.code: sp for sp in TreeSpecies.objects.filter(historic=False)}
        self.species_dict_hist = {sp.code: sp for sp in TreeSpecies.objects.filter(historic=True)}
        self.vita_dict = {vita.code: vita for vita in Vita.objects.all()}
        self.marker_types_dict = {mark.code: mark for mark in MarkerType.objects.all()}

    def species_from_code(self, code, dbh08):
        code = code.upper()
        if code == 'UU':
            code = 'U'
        elif code == 'NN':
            code = 'N'
        elif code == 'UE':
            # Error, see https://gitlab.com/guaraci/ksp-zg/-/issues/20
            code = 'U'
        if not dbh08 and code in self.species_dict_hist:
            # A, E and K can be historic values if not in the 08/10 inventory.
            return self.species_dict_hist[code]
        return self.species_dict[code]

    def vita_from_values(self, durr, remarks):
        if durr.lower() == 'd':
            return self.vita_dict['D']  # Dürrständer
        remarks_vals = [val.strip() for val in remarks.split(';')]
        if 'E' in remarks_vals:
            return self.vita_dict['E']  # Einwuchs
        if 'N' in remarks_vals or "K (Rodung)" in remarks_vals:
            return self.vita_dict['N']  # Nutzung
        return self.vita_dict['L']  # Lebend

    def extract_remarks(self, remarks):
        for val in remarks.split(';'):
            if val.strip().startswith('K'):
                return val.strip()
        return ''

    def import_lines(self):
        errs = set()
        for idx, line in enumerate(line_from_table(self.db, access_tables['baumdaten'])):
            try:
                with transaction.atomic():
                    self.import_line(line)
            except (KeyError, ValidationError) as err:
                print(line)
                errs.add(str(err))
            if idx % 1000 == 0:
                print(f"{idx} tree lines imported.")
        if errs:
            print(errs)

    def import_line(self, line):
        plot_key = str(int(line['KEY']))
        try:
            plot = Plot.objects.get(nr=plot_key)
        except Plot.DoesNotExist:
            print(f"No existing plot with nr={plot_key}")
            return
        tree_key = line['TS BAUMKEY'][1:]
        if line['BAUMART'] in self.marker_types_dict:
            if Marker.objects.filter(nr=tree_key).exists():
                return  # Already imported
            Marker.objects.create(
                plot=plot,
                nr=tree_key,
                mtype=self.marker_types_dict[line['BAUMART']],
                azimuth=to_int(line['AZIMUT']),
                distance=to_int(line['DISTANZ']),
            )
            return

        plotobs_dict = {obs.year: obs for obs in plot.plotobs_set.all()}

        def obs_from_period(period):
            if period == '72/74':
                obs = plotobs_dict.get(1972, plotobs_dict.get(1973, plotobs_dict.get(1974)))
            elif period == '90/92':
                obs = plotobs_dict.get(1990, plotobs_dict.get(1991, plotobs_dict.get(1992, plotobs_dict.get(1993))))
            elif period == '93':
                obs = plotobs_dict.get(1993)
            elif period == '08/10':
                obs = plotobs_dict.get(2008, plotobs_dict.get(2009, plotobs_dict.get(2010)))
            else:
                raise Exception(f"Unknown period {period}")
            if obs is None:
                # if dbh for that period is empty or 0 and K (Rodung) in comments, then OK, just ignore.
                dbh = line[f'BHD {period}']
                if not dbh and line[f'BEM {period}'].strip() in ("K (Rodung)", "K (Aufnahme fehlt)", "K (Rutsch)"):
                    return None

                if period == '08/10' and str(line['KEY']).startswith('10'):
                    # Those special plots (false coords) were never inventoried in 08/10.
                    return None

                if period == '08/10' and plot.absent and plot.absent.description == 'Rodung':
                    return None

                # missing 1993 plotobs should be auto-created
                # https://gitlab.com/guaraci/ksp-zg/-/issues/21
                if period == '93':
                    obs91 = plotobs_dict.get(1990, plotobs_dict.get(1991))
                    return PlotObs.objects.create(
                        plot=plot,
                        year=1993,
                        quarter=4,
                        status=PlotObsStatus.objects.get(code=2),
                        # E-CODE, always identical to 90/92 code:
                        municipality=obs91.municipality,
                        owner=obs91.owner,
                        division=obs91.division,
                    )

                msg = f"Unable to find a plotobs line for {tree_key} and period {period}"
                print(msg)
                return None
            return obs

        if Tree.objects.filter(nr=tree_key).exists():
            return  # Already imported
        tree = Tree(
            plot=plot,
            nr=tree_key,
            azimuth=to_int(line['AZIMUT']),
            distance=to_int(line['DISTANZ']),
            spec=self.species_from_code(line['BAUMART'] + line['BAUMART-2STELLE'], line['SPBAUM 08/10']),
            mirror=('*' in line['SPBAUM 72/74'] + line['SPBAUM 90/92'] + line['SPBAUM 93'] + line['SPBAUM 08/10']),
        )
        tree.full_clean()
        tree.save()

        objs = []
        if line['BHD 72/74']:
            obs = obs_from_period('72/74')
            if obs:
                tobs = TreeObs(
                    obs=obs,
                    tree=tree,
                    dbh=to_int(line['BHD 72/74']),
                    vita=self.vita_from_values(line['DUERR 72/74'], ''),
                    remarks='',
                )
                tobs.full_clean()
                objs.append(tobs)

        if line['BHD 90/92'] or line['BEM 90/92']:
            obs = obs_from_period('90/92')
            if obs:
                tobs = TreeObs(
                    obs=obs,
                    tree=tree,
                    dbh=to_int(line['BHD 90/92']),
                    vita=self.vita_from_values(line['DUERR 90/92'], line['BEM 90/92']),
                    remarks=self.extract_remarks(line['BEM 90/92']),
                )
                tobs.full_clean()
                objs.append(tobs)

        if line['BHD 93'] or line['BEM 93']:
            obs = obs_from_period('93')
            if obs:
                tobs = TreeObs(
                    obs=obs,
                    tree=tree,
                    dbh=to_int(line['BHD 93']),
                    vita=self.vita_from_values(line['DUERR 93'], line['BEM 93']),
                    remarks='',
                )
                tobs.full_clean()
                objs.append(tobs)

        if line['BHD 08/10'] or (line['BEM 08/10'] and line['BEM 08/10'] != 'K (Aufnahme fehlt)'):
            obs = obs_from_period('08/10')
            if obs:
                tobs = TreeObs(
                    obs=obs,
                    tree=tree,
                    dbh=to_int(line['BHD 08/10']),
                    vita=self.vita_from_values(line['DUERR 08/10'], line['BEM 08/10']),
                    remarks=self.extract_remarks(line['BEM 08/10']),
                )
                tobs.full_clean()
                objs.append(tobs)

        TreeObs.objects.bulk_create(objs)


if __name__ == "__main__":
    os.environ['DJANGO_SETTINGS_MODULE'] = 'common.settings'
    django.setup()

    parser = argparse.ArgumentParser()
    parser.add_argument("accessdb", help="Path to the original Access database")
    parser.add_argument("--recreate-db", action='store_true')
    args = parser.parse_args()

    if args.recreate_db:
        subprocess.run(['psql', '-d', 'postgres', '-c', 'DROP DATABASE ksp_zg;'])

    res = subprocess.run(['psql', '-d', 'postgres', '-c', 'SELECT datname FROM pg_database;'], capture_output=True)
    if not 'ksp_zg' in res.stdout.decode():
        subprocess.run(['psql', '-d', 'postgres', '-c', 'CREATE DATABASE ksp_zg;'])
        subprocess.run(['psql', '-d', 'ksp_zg', '-c', 'CREATE EXTENSION IF NOT EXISTS postgis;'])
        call_command('migrate')
        call_command('loaddata', 'municipality.json')
    
    from ksp_zg.models import *

    db = AccessParser(args.accessdb)
    #Plot.objects.all().delete()
    importer = PlotImporter(db, only_missing=not args.recreate_db)
    importer.import_lines()
    importer.import_mutations()
    #Tree.objects.all().delete()
    TreeImporter(db).import_lines()
