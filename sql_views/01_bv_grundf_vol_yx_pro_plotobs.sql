CREATE OR REPLACE VIEW bv_grundf_vol_yx_pro_plotobs AS
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.id AS plot_obs_id,
    plot_obs.year,
    plot_obs.inv_period,
    plot_obs.municipality_id,
    10000 AS density,
    COALESCE(subq.anzahl_probebaeume, 0) AS "Anzahl Probebaeume",
    COALESCE(subq.volumen_m3, 0) AS "Volumen m3",
    COALESCE(subq.grundflaeche_m2, 0) AS "Grundflaeche m2",
    COALESCE(subq.anzahl_probebaeume::numeric * ksp_lokale_dichte(0), 0) AS "Stammzahl pro ha",
    COALESCE(subq.volumen_m3 * ksp_lokale_dichte(0), 0) AS "Volumen pro ha",
    COALESCE(subq.grundflaeche_m2 * ksp_lokale_dichte(0), 0) AS "Grundflaeche pro ha"
   FROM ( SELECT bv.obs_id,
            count(*) AS anzahl_probebaeume,
            sum(bv.volume) AS volumen_m3,
            sum(bv.surface) AS grundflaeche_m2
           FROM public.bv_grundf_vol_pro_treeobs bv
          WHERE (bv.code = 'L' OR bv.code = 'E') AND bv.dbh > 11
          GROUP BY bv.obs_id) subq
     RIGHT JOIN plot_obs ON subq.obs_id = plot_obs.id
   WHERE NOT plot_obs.inv_spezial;
