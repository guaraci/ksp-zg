CREATE OR REPLACE VIEW public.bv_zuwachs_einwuchs_pro_plotobs AS 
 SELECT po.id,
    max(po.plot_id) AS plot_id,
    COALESCE(sum(zuw.growth_year), 0::double precision) AS growth,
    COALESCE(sum(zuw.growth_year) * ksp_lokale_dichte(0)::double precision, 0) AS growth_ha,
    count(zuw.obs_id) AS nb_stem,
    sum(zuw.growth_year) / count(*)::double precision * ksp_lokale_dichte(0)::double precision AS growth_stem_ha
   FROM bv_secondobs po
     LEFT JOIN bv_zuwachs_einwuchs zuw ON zuw.obs_id = po.id
  GROUP BY po.id;
