CREATE OR REPLACE VIEW public.bv_grundf_vol_yx_pro_plotobs_und_allspec_nutzung AS
 SELECT (sub2.plot_obs_id || '-'::text) || sub2.tree_spec_id AS id,
    sub2.plot_id,
    sub2.plot_obs_id,
    sub2.year,
    sub2.tree_spec_id,
    sub2.municipality_id,
    COALESCE(subq.anzahl_probebaeume, 0) AS "Anzahl Probebaeume",
    COALESCE(subq.volumen, 0) AS "Volumen m3",
    COALESCE(subq.grundflaeche, 0) AS "Grundflaeche m2",
    COALESCE(subq.anzahl_probebaeume::numeric * ksp_lokale_dichte(0), 0) AS "Stammzahl pro ha",
    COALESCE(subq.volumen * ksp_lokale_dichte(0), 0) AS "Volumen pro ha",
    COALESCE(subq.grundflaeche * ksp_lokale_dichte(0), 0) AS "Grundflaeche pro ha"
   FROM ( SELECT tobsb.obs_id,
            bv.spec_id,
            count(*) AS anzahl_probebaeume,
            sum(bv.volume) AS volumen,
            sum(bv.surface) AS grundflaeche
           FROM public.bv_grundf_vol_pro_treeobs bv
             JOIN tree_obs tobsb ON bv.tree_id = tobsb.tree_id
             LEFT JOIN lt_vita ON tobsb.vita_id = lt_vita.id
             LEFT JOIN plot_obs po_new ON bv.obs_id = po_new.id
             LEFT JOIN plot_obs po_old ON tobsb.obs_id = po_old.id
         WHERE lt_vita.code = 'N' AND po_new.inv_period = (po_old.inv_period - 1)
          GROUP BY tobsb.obs_id, bv.spec_id) subq
     RIGHT JOIN ( SELECT plot_obs.id AS plot_obs_id,
            plot_obs.plot_id,
            plot_obs.year,
            plot_obs.municipality_id,
            lt_tree_spec.id AS tree_spec_id
           FROM plot_obs, lt_tree_spec
           WHERE NOT plot_obs.inv_spezial) sub2 ON sub2.plot_obs_id = subq.obs_id AND sub2.tree_spec_id = subq.spec_id
  ORDER BY subq.volumen * ksp_lokale_dichte(0);
