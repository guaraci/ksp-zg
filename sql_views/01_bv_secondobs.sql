CREATE OR REPLACE VIEW public.bv_secondobs AS 
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.year,
    plot_obs.municipality_id,
    plot_obs.inv_period as invent1,
    prev_obs.inv_period as invent0,
    ksp_vegetationperiods(prev_obs.year, prev_obs.quarter, plot_obs.year, plot_obs.quarter) AS year_diff
   FROM plot_obs
     LEFT JOIN ( SELECT pobs2.id,
            pobs2.plot_id,
            pobs2.year,
            pobs2.quarter,
            pobs2.inv_period
           FROM plot_obs pobs2
           WHERE NOT pobs2.inv_spezial
        ) prev_obs ON plot_obs.plot_id = prev_obs.plot_id AND plot_obs.inv_period = (prev_obs.inv_period + 1)
  WHERE NOT plot_obs.inv_spezial AND prev_obs.id IS NOT NULL;
