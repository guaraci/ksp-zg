CREATE OR REPLACE VIEW public.bv_zuwachs_lebend AS
 SELECT bv_a.id,
    bv_a.tree_id,
    bv_a.obs_id,
    pob.inv_period AS invent0,
    poa.inv_period AS invent1,
    bv_b.dbh AS dbh0,
    bv_a.dbh AS dbh1,
    bv_b.volume AS vol0,
    bv_a.volume AS vol1,
    bv_b.surface AS grundf0,
    bv_a.surface AS grundf1,
    bv_a.dbh - bv_b.dbh AS dbh_diff,
    ksp_vegetationperiods(pob.year,pob.quarter,poa.year, poa.quarter) AS year_diff,
    bv_a.surface - bv_b.surface AS grundf_diff,
    GREATEST(0::double precision, (bv_a.volume - bv_b.volume)::double precision / NULLIF(ksp_vegetationperiods(pob.year,pob.quarter,poa.year, poa.quarter), 0)::double precision) AS growth_year
   FROM public.bv_grundf_vol_pro_treeobs bv_a
     JOIN public.bv_grundf_vol_pro_treeobs bv_b ON bv_a.tree_id = bv_b.tree_id AND NOT bv_a.inv_spezial AND NOT bv_b.inv_spezial
     LEFT JOIN plot_obs poa ON bv_a.obs_id = poa.id
     LEFT JOIN plot_obs pob ON bv_b.obs_id = pob.id
  WHERE bv_a.code = 'L' AND (bv_b.code = ANY (ARRAY['E', 'L'])) AND poa.inv_period = (pob.inv_period + 1);
