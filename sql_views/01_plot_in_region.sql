CREATE MATERIALIZED VIEW IF NOT EXISTS public.plot_in_region AS
 SELECT row_number() OVER ()::integer AS id,
    plot.id AS plot_id,
    adminregion.id AS adminregion_id,
    adminregion.name AS region_name,
    adminregion.region_type AS region_type
   FROM plot, adminregion
  WHERE st_within(plot.center, adminregion.geom)
WITH DATA;
