CREATE OR REPLACE VIEW public.bv_web_homepage_holzproduktion
 AS
 SELECT row_number() OVER () AS id,
    m.name,
    b.inv_period,
    string_agg(DISTINCT b.year::text, '-'::text) AS year,
    sum(b."Anzahl Probebaeume")::integer AS "Anzahl Probebaeume",
    avg(b."Stammzahl pro ha")::integer AS "Stammzahl pro ha",
    stddev(b."Stammzahl pro ha")::double precision / sqrt(count(*)::double precision) / avg(b."Stammzahl pro ha")::double precision * 100::double precision AS "% Standardfehler",
    avg(b."Volumen pro ha")::numeric(5,1) AS "Volumen pro ha",
    stddev(b."Volumen pro ha")::double precision / sqrt(count(*)::double precision) / avg(b."Volumen pro ha")::double precision * 100::double precision AS "% Standardfehler2",
    avg(b."Grundflaeche pro ha")::numeric(5,1) AS "Grundflaeche pro ha",
    stddev(b."Grundflaeche pro ha") / sqrt(count(*)::double precision) / avg(b."Grundflaeche pro ha") * 100::double precision AS "% Standardfehler3",
    sum(ksp_lokale_dichte(0) * 0.03 * b.density / 10000)::numeric(5,1) AS "theoretische Waldfläche ha",
    count(b.inv_period) AS "Anzahl Probepunkte"
   FROM bv_grundf_vol_yx_pro_plotobs b
     LEFT JOIN municipality m on b.municipality_id = m.id
  GROUP BY m.name, b.inv_period
  ORDER BY m.name, b.inv_period;
