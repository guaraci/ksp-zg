CREATE OR REPLACE VIEW bv_stem_pro_plotobs_pro_vita AS
 SELECT tree_obs.obs_id AS id,
    count(*) AS total_stem,
    sum(
        CASE
            WHEN lt_vita.code = 'L' THEN 1
            ELSE 0
        END) AS lebend,
    sum(
        CASE
            WHEN lt_vita.code = 'E' THEN 1
            ELSE 0
        END) AS einwuchs,
    sum(
        CASE
            WHEN lt_vita.code = 'N' THEN 1
            ELSE 0
        END) AS nutzung,
    sum(
        CASE
            WHEN lt_vita.code = 'D' THEN 1
            ELSE 0
        END) AS tot
   FROM tree_obs
     LEFT JOIN plot_obs ON tree_obs.obs_id = plot_obs.id
     LEFT JOIN lt_vita ON tree_obs.vita_id = lt_vita.id
  GROUP BY tree_obs.obs_id;
