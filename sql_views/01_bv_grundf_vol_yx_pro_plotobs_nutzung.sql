CREATE OR REPLACE VIEW bv_grundf_vol_yx_pro_plotobs_nutzung AS
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.id AS plot_obs_id,
    plot_obs.year,
    plot_obs.inv_period,
    plot_obs.municipality_id,
    10000 AS density,
    COALESCE(subq.anzahl_probebaeume, 0) AS "Anzahl Probebaeume",
    COALESCE(subq.volumen, 0) AS "Volumen m3",
    COALESCE(subq.grundflaeche, 0) AS "Grundflaeche m2",
    COALESCE(subq.anzahl_probebaeume::numeric * ksp_lokale_dichte(0), 0) AS "Stammzahl pro ha",
    COALESCE(subq.volumen * ksp_lokale_dichte(0), 0) AS "Volumen pro ha",
    COALESCE(subq.grundflaeche * ksp_lokale_dichte(0), 0) AS "Grundflaeche pro ha"
   FROM ( SELECT bv.obs_id,
            count(*) AS anzahl_probebaeume,
            sum(ksp_tarif_volume(tobsb.dbh, lt_tree_spec.tarif_klass20)) AS volumen,
            sum(ksp_grundflaeche_bl(tobsb.dbh)) AS grundflaeche
           FROM public.bv_grundf_vol_pro_treeobs bv
             JOIN tree_obs tobsb ON bv.tree_id = tobsb.tree_id
             JOIN tree ON tobsb.tree_id = tree.id
             JOIN lt_tree_spec ON tree.spec_id = lt_tree_spec.id
             LEFT JOIN plot_obs po_new ON bv.obs_id = po_new.id
             LEFT JOIN plot_obs po_old ON tobsb.obs_id = po_old.id
          WHERE bv.code = 'N' AND po_new.inv_period = (po_old.inv_period + 1)
          GROUP BY bv.obs_id) subq
     FULL JOIN plot_obs ON subq.obs_id = plot_obs.id
   WHERE NOT plot_obs.inv_spezial;
