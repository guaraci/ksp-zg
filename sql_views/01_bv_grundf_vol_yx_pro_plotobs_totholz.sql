CREATE OR REPLACE VIEW bv_grundf_vol_yx_pro_plotobs_totholz AS
 SELECT plot_obs.id,
    plot_obs.plot_id,
    plot_obs.id AS plot_obs_id,
    plot_obs.year,
    plot_obs.municipality_id,
    COALESCE(subq."Anzahl Probebaeume", 0) AS "Anzahl Probebaeume",
    COALESCE(subq."Volumen m3", 0) AS "Volumen m3",
    COALESCE(subq."Grundflaeche m2", 0) AS "Grundflaeche m2",
    COALESCE(subq."Anzahl Probebaeume"::numeric * ksp_lokale_dichte(0), 0) AS "Stammzahl pro ha",
    COALESCE(subq."Volumen m3" * ksp_lokale_dichte(0), 0) AS "Volumen pro ha",
    COALESCE(subq."Grundflaeche m2" * ksp_lokale_dichte(0)::double precision, 0) AS "Grundflaeche pro ha"
   FROM ( SELECT bv.obs_id,
            count(*) AS "Anzahl Probebaeume",
            sum(bv.volume) AS "Volumen m3",
            sum(bv.surface) AS "Grundflaeche m2"
           FROM public.bv_grundf_vol_pro_treeobs bv
          WHERE bv.code = 'D'
          GROUP BY bv.obs_id) subq
     FULL JOIN plot_obs ON subq.obs_id = plot_obs.id
   WHERE NOT plot_obs.inv_spezial;
