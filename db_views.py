from django.db import models


class HomepageView(models.Model):
    gemeinde = models.CharField(max_length=50, db_column="name")
    inv_period = models.SmallIntegerField()
    jahr = models.SmallIntegerField("Aufnahmejahr", db_column="year")
    probepunkte = models.IntegerField("Anzahl Probepunkte", db_column="Anzahl Probepunkte")
    waldflaeche = models.DecimalField("theoretische Waldfläche pro ha", max_digits=5, decimal_places=1,
        db_column="theoretische Waldfläche ha")
    probebaum_abs = models.IntegerField("Anzahl Probebäume", db_column="Anzahl Probebaeume")
    stammzahl_ha = models.IntegerField("Stammzahl pro ha", db_column="Stammzahl pro ha")
    stammzahl_stdf = models.FloatField("Sf [%]", db_column="%% Standardfehler")
    volumen_ha = models.DecimalField("Volumen [m3 pro ha]", max_digits=5, decimal_places=1,
        db_column="Volumen pro ha")
    volumen_stdf = models.FloatField("Sf [%]", db_column="%% Standardfehler2")
    grundflaeche_ha = models.DecimalField("Grundflaeche [m2 pro ha]", max_digits=5, decimal_places=1,
        db_column="Grundflaeche pro ha")
    grundflaeche_stdf = models.FloatField("Sf [%]", db_column="%% Standardfehler3")

    class Meta:
        db_table = 'bv_web_homepage_holzproduktion'
        managed = False
