from django.http import QueryDict
from django.test import TestCase

from ksp_zg.models import Plot
from ksp_zg.queries import PerimeterSet


class GeneralTests(TestCase):
    def test_plot_radius(self):
        self.assertEqual(Plot(slope=20).radius, 10)
        self.assertEqual(Plot(slope=25).radius, 10.1)
        self.assertEqual(Plot(slope=60).radius, 10.8)

    def test_perimgroup_pks_from_request(self):
        # All possible query parameters targeting perimeter groups
        qdict = {
            'gem': 3, 'frevier': 3, 'warm': 3, 'frei': 3,
            'eigentumer': 3, 'eigentumer-current': 3,
            'erhw': 3, 'nat': 2, 'schw': 1
        }
        qdict = QueryDict('&'.join(f'{k}={v}' for k, v in qdict.items())) 
        for perimgroup in PerimeterSet.get_perimeters():
            with self.subTest(perimgroup=perimgroup):
                if hasattr(perimgroup, 'adminr_keys'):
                    self.assertTrue(perimgroup.is_adminregion)
                    self.assertEqual(perimgroup.pks_from_request(qdict), [3, 2, 1])
                else:
                    self.assertEqual(perimgroup.pks_from_request(qdict), [3])
