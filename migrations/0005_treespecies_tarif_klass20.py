from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0004_fill_inv_period'),
    ]

    operations = [
        migrations.AddField(
            model_name='treespecies',
            name='tarif_klass20',
            field=models.CharField(blank=True, choices=[('fichte', 'Fichte (HAHBART1)'), ('tanne', 'Tanne (HAHBART2)'), ('buche', 'Buche (HAHBART7)'), ('ubrige_laub', 'übrige Laubhölzer (HAHBART12)'), ('ubrige_nad', 'übrige Nadelhölzer (HAHBART6)')], max_length=15),
        ),
    ]
