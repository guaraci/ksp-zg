from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0009_adminregion'),
    ]

    operations = [
        migrations.RenameField(
            model_name='owner',
            old_name='description',
            new_name='name',
        ),
        migrations.AlterField(
            model_name='owner',
            name='name',
            field=models.CharField(max_length=80, verbose_name='Name'),
        ),
    ]
