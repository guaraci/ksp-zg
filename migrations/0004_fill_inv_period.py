from django.db import migrations


def fill_inv_period(apps, schema_editor):
    PlotObs = apps.get_model('ksp_zg', 'PlotObs')
    # Previous migration defined 1 as default value
    PlotObs.objects.filter(year__gt=1975, year__lt=1995).update(inv_period=2)
    PlotObs.objects.filter(year__gt=2000, year__lt=2015).update(inv_period=3)
    PlotObs.objects.filter(year=1993, quarter=4).update(inv_spezial=True)


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0003_plotobs_inv_period_inv_spezial'),
    ]

    operations = [
        migrations.RunPython(fill_inv_period, migrations.RunPython.noop)
    ]
