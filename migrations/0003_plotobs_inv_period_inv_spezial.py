from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0002_fill_lookup_tables'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotobs',
            name='inv_period',
            field=models.PositiveSmallIntegerField(
                default=1, verbose_name='Aufnahmeperiode',
                choices=[
                    (1, 'Inventur 1 (1972-1974)'),
                    (2, 'Inventur 2 (1991-1993)'),
                    (3, 'Inventur 3 (2008-2010)'),
                    (4, 'Inventur 4 (2022?-?)')
                ],
            ),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='plotobs',
            name='inv_spezial',
            field=models.BooleanField(default=False, verbose_name='Spezialinventur'),
        ),
    ]
