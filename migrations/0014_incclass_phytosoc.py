from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0013_treeobsextra'),
    ]

    operations = [
        migrations.CreateModel(
            name='IncClass',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.PositiveSmallIntegerField(unique=True)),
                ('description', models.CharField(max_length=20, verbose_name='Beschreibung')),
            ],
            options={
                'verbose_name': 'Ertragsklasse',
                'verbose_name_plural': 'Ertragsklassen',
                'db_table': 'lt_incclass',
            },
        ),
        migrations.CreateModel(
            name='Phytosoc',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=6, unique=True)),
                ('description', models.CharField(max_length=200, verbose_name='Beschreibung')),
                ('inc_class', models.ForeignKey(on_delete=models.deletion.PROTECT, to='ksp_zg.incclass', verbose_name='Ertragsklasse')),
            ],
            options={
                'verbose_name': 'Phytosoziologie',
                'verbose_name_plural': 'Phytosoziologie',
                'db_table': 'phytosociology',
            },
        ),
    ]
