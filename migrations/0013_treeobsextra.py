from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0012_renumber_trees'),
    ]

    operations = [
        migrations.CreateModel(
            name='TreeObsExtra',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('d7', models.PositiveSmallIntegerField(verbose_name='D7M')),
                ('hoehe', models.PositiveSmallIntegerField(verbose_name='Höhe')),
                ('tree_obs', models.OneToOneField(on_delete=models.deletion.CASCADE, related_name='+', to='ksp_zg.treeobs')),
            ],
            options={
                'db_table': 'tree_obs_extra',
            },
        ),
    ]
