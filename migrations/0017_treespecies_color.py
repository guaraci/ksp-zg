import colorful.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0016_waldbestandkarte'),
    ]

    operations = [
        migrations.AddField(
            model_name='treespecies',
            name='color',
            field=colorful.fields.RGBColorField(blank=True, verbose_name='Farbe'),
        ),
    ]
