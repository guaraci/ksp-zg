import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ('ksp_zg', '0015_forestsocio'),
    ]

    operations = [
        migrations.CreateModel(
            name='WaldBestandKarte',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('remarks', models.TextField(blank=True, verbose_name='Bemerkungen')),
                ('geom', django.contrib.gis.db.models.fields.PolygonField(srid=2056)),
                ('entwicklungstufe', models.ForeignKey(blank=True, db_column='es', null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_zg.develstage')),
                ('mischungsgrad', models.ForeignKey(blank=True, db_column='mg', null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_zg.forestmixture')),
                ('schlussgrad', models.ForeignKey(blank=True, db_column='sg', null=True, on_delete=django.db.models.deletion.SET_NULL, to='ksp_zg.crownclosure')),
            ],
            options={
                'db_table': 'waldbestandkarte',
            },
        ),
    ]
