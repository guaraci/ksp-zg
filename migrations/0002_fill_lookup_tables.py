from django.db import migrations


def fill_tables(apps, schema_editor):
    PlotAbsence = apps.get_model('ksp_zg', 'PlotAbsence')
    PlotObsStatus = apps.get_model('ksp_zg', 'PlotObsStatus')
    Mutation = apps.get_model('ksp_zg', 'Mutation')
    DevelStage = apps.get_model('ksp_zg', 'DevelStage')
    CrownClosure = apps.get_model('ksp_zg', 'CrownClosure')
    ForestMixture = apps.get_model('ksp_zg', 'ForestMixture')
    Division = apps.get_model('ksp_zg', 'Division')
    OwnerCategory = apps.get_model('ksp_zg', 'OwnerCategory')
    OwnerType = apps.get_model('ksp_zg', 'OwnerType')
    Owner = apps.get_model('ksp_zg', 'Owner')
    HerbLayer = apps.get_model('ksp_zg', 'HerbLayer')
    Landslide = apps.get_model('ksp_zg', 'Landslide')
    MecDamage = apps.get_model('ksp_zg', 'MecDamage')
    MarkerType = apps.get_model('ksp_zg', 'MarkerType')
    TreeSpecies = apps.get_model('ksp_zg', 'TreeSpecies')
    Vita = apps.get_model('ksp_zg', 'Vita')

    PlotAbsence.objects.bulk_create([
        PlotAbsence(description='Rodung'),
        PlotAbsence(description='Fehlerhafte Waldausscheidung'),
        PlotAbsence(description='Aufnahme fehlt'),
        PlotAbsence(description='Wald unzugänglich'),
    ])
    PlotObsStatus.objects.bulk_create([
        #0=letzte Inventur Abgang?
        PlotObsStatus(code=1, description='Erstaufnahme'),
        PlotObsStatus(code=2, description='Folgeaufnahme'),
        PlotObsStatus(code=3, description='Abgang (Rodung etc.)'),
    ])
    Mutation.objects.bulk_create([
        Mutation(code='W', short_descr='Wechsel', long_descr='Änderung des Eigentümercodes, Eigentumsänderung'),
        Mutation(code='A', short_descr='Zugang', long_descr='Aufforstung'),
        Mutation(code='X', short_descr='Neu kartiert', long_descr='Zugang gemäss neuer Bestandeskarte oder Bodenbedeckung'),
        Mutation(code='R', short_descr='Abgang', long_descr='Rodung und gleichzeitige Entlassung aus dem Waldareal'),
        Mutation(code='Y', short_descr='Entlassen', long_descr='Kein Wald gemäss neuer Bestandeskartierung oder Bodenbedeckung'),
        Mutation(code='S', short_descr='Spiegel geändert', long_descr='Aufnahme alte Situation (Berechnung Zuwachs/Nutzung)'),
        Mutation(code='K', short_descr='Spiegel geändert', long_descr='Aufnahme neue Situation (Berechnung Stammzahl/Vorrat)'),
        Mutation(code='V', short_descr='Verschiebung geändert', long_descr='Aufnahme am alten Zentrum (Berechnung Zuwachs/Nutzung)'),
        Mutation(code='L', short_descr='Verschiebung geändert', long_descr='Aufnahme am neuen Zentrum (Berechnung Stammzahl/Vorrat)'),
        Mutation(code='E', short_descr='?', long_descr='?'),
    ])
    DevelStage.objects.bulk_create([
        DevelStage(code=0, description='Gebüsch- und Baumgruppen'),
        DevelStage(code=100, description='Jungwuchs und Dickung'),
        DevelStage(code=200, description='Stangenholz'),
        DevelStage(code=300, description='Baumholz I'),
        DevelStage(code=400, description='Baumholz II'),
        DevelStage(code=500, description='Starkholz'),
        DevelStage(code=600, description='Blössen (andere Vegetationsformen als Wald;Sumpf, Wiese, etc.)'),
        DevelStage(code=700, description='Unproduktiv (Vegetationslos; Fels, Kiesgrube,Lagerplätze, etc.)'),
        DevelStage(code=800, description='Stufige Bestände'),
        DevelStage(code=900, description='Ufer- und Kleingehölze'),
    ])
    ForestMixture.objects.bulk_create([
        ForestMixture(code=0, description='unbestimmt'),
        ForestMixture(code=10, description='Nadelholz rein'),
        ForestMixture(code=20, description='Nadelholz vorherrschend'),
        ForestMixture(code=30, description='Laubholz rein'),
        ForestMixture(code=40, description='Laubholz vorherrschend'),
        ForestMixture(code=60, description='? Error?'),  # https://gitlab.com/guaraci/ksp-zg/-/issues/8
        ForestMixture(code=70, description='Keine Bestockung'),
    ])
    CrownClosure.objects.bulk_create([
        CrownClosure(code=0, description='unbestimmt'),
        CrownClosure(code=1, description='gedrängt'),
        CrownClosure(code=2, description='geschlossen'),
        CrownClosure(code=3, description='lückig'),
        CrownClosure(code=4, description='aufgelöst'),
        CrownClosure(code=5, description='Überhälter'),
        CrownClosure(code=6, description='stufig'),
        CrownClosure(code=7, description='unbestockt'),
        CrownClosure(code=9, description='? Error?'),
    ])
    Division.objects.bulk_create([
        Division(code='1', description='Revier 1'),
        Division(code='2', description='Revier 2'),
        Division(code='3', description='Revier 3'),
        # https://gitlab.com/guaraci/ksp-zg/-/issues/7
        Division(code='4', description='Zug Revier Chiemen'),
        Division(code='5', description='Revier ??'),
        Division(code='T', description='keine Unterteilung'),
    ])
    offentlich, privat, staatswald = OwnerCategory.objects.bulk_create([
        OwnerCategory(description='öffentlich'),
        OwnerCategory(description='privat'),
        OwnerCategory(description='Staatswald'),
    ])
    types = OwnerType.objects.bulk_create([
        OwnerType(code='1', description='Bund', categ=offentlich),
        OwnerType(code='2', description='Kanton (Staatswald)', categ=staatswald),
        OwnerType(code='3', description='Einwohnergemeinden', categ=offentlich),
        OwnerType(code='4', description='Bürgergemeinden', categ=offentlich),
        OwnerType(code='5', description='Kirchgemeinden', categ=offentlich),
        OwnerType(code='6', description='Korporationen', categ=offentlich),
        OwnerType(code='K', description='Klöster', categ=privat),
        OwnerType(code='W', description='Waldgenossenschaften', categ=privat),
        OwnerType(code='P', description='Übrige Private', categ=privat),
    ])
    bund = types[0]
    kanton = types[1]
    einw = types[2]
    burger = types[3]
    kirch = types[4]
    korp = types[5]
    kloster = types[6]
    waldgen = types[7]
    priv = types[8]
    Owner.objects.bulk_create([
        Owner(code='1', description='SBB', typ=bund),
        Owner(code='2', description='ETH (Chamau)', typ=bund),
        Owner(code='3', description='ETH (Frühbühl)', typ=bund),
        Owner(code='4', description='EMD (Gubel)', typ=bund),
        Owner(code='5', description='EMD (E+Z)', typ=bund),
        Owner(code='S', description='Staatswald', typ=kanton),

        Owner(code='Z', description='Einwohnergemeinde Zug', typ=einw),
        Owner(code='C', description='Einwohnergemeinde Cham', typ=einw),
        Owner(code='O', description='Einwohnergemeinde Oberägeri', typ=einw),
        Owner(code='R', description='Einwohnergemeinde Risch', typ=einw),
        Owner(code='Y', description='Einwohnergemeinde Steinhausen', typ=einw),

        Owner(code='B', description='Bürgergemeinde Baar', typ=burger),
        Owner(code='C', description='Bürgergemeinde Cham', typ=burger),
        Owner(code='K', description='Bürgergemeinde Knonau', typ=burger),
        Owner(code='M', description='Bürgergemeinde Maschwanden', typ=burger),

        Owner(code='B', description='Kirchgemeinde Baar', typ=kirch),
        Owner(code='N', description='Kirchgemeinde Neuheim', typ=kirch),
        Owner(code='R', description='Kirchgemeinde Risch', typ=kirch),

        Owner(code='Z', description='Korporation Zug', typ=korp),
        Owner(code='O', description='Korporation Oberägeri', typ=korp),
        Owner(code='U', description='Korporation Unterägeri', typ=korp),
        Owner(code='B', description='Korporation Baar-Dorf', typ=korp),
        Owner(code='D', description='Korporation Deinikon', typ=korp),
        Owner(code='L', description='Korporation Blickensdorf', typ=korp),
        Owner(code='G', description='Korporation Grüt', typ=korp),
        Owner(code='I', description='Korporation Inwil', typ=korp),
        Owner(code='H', description='Korporation Hünenberg', typ=korp),
        Owner(code='W', description='Korporation Walchwil', typ=korp),
        Owner(code='M', description='Korporation Maschwanden', typ=korp),
        Owner(code='Q', description='Korporation Uerzlikon', typ=korp),
        Owner(code='T', description='Gen. Steinerberg', typ=korp),
        Owner(code='P', description='Oberallmeindkorporation Schwyz', typ=korp),
        Owner(code='N', description='Genossame Steinen', typ=korp),
        Owner(code='A', description='Genossame Sattel', typ=korp),

        Owner(code='A', description='Kloster Maria Opferung', typ=kloster),
        Owner(code='X', description='Kloster Gubel', typ=kloster),
        Owner(code='F', description='Kloster Frauenthal', typ=kloster),
        Owner(code='H', description='Institut Heiligkreuz', typ=kloster),
        Owner(code='I', description='Institut Lehrschwestern Menzingen', typ=kloster),
        Owner(code='V', description='Kloster Maria Opferung Zug', typ=kloster),

        Owner(code='S', description='Waldgenossenschaft Steinhausen', typ=waldgen),
        Owner(code='C', description='Waldgenossenschaft Städtli Cham', typ=waldgen),
        Owner(code='Y', description='Waldgenossenschaft Steinhausen', typ=waldgen),

        Owner(code='Z', description='Privatwald Gemeinde Zug', typ=priv),
        Owner(code='C', description='Privatwald Gemeinde Cham', typ=priv),
        Owner(code='O', description='Privatwald Gemeinde Oberägeri', typ=priv),
        Owner(code='U', description='Privatwald Gemeinde Unterägeri', typ=priv),
        Owner(code='B', description='Privatwald Gemeinde Baar', typ=priv),
        Owner(code='H', description='Privatwald Gemeinde Hünenberg', typ=priv),
        Owner(code='W', description='Privatwald Gemeinde Walchwil', typ=priv),
        Owner(code='M', description='Privatwald Gemeinde Menzingen', typ=priv),
        Owner(code='N', description='Privatwald Gemeinde Neuheim', typ=priv),
        Owner(code='R', description='Privatwald Gemeinde Risch', typ=priv),
    ])
    HerbLayer.objects.bulk_create([
        HerbLayer(code=0, description='Keine Krautschicht vorhanden'),
        HerbLayer(code=1, description='Die Krautschicht bedeckt ein Viertel (1/4) der Fläche.'),
        HerbLayer(code=2, description='Die Krautschicht bedeckt zwei Viertel (2/4) der Fläche.'),
        HerbLayer(code=3, description='Die Krautschicht bedeckt drei Viertel (3/4) der Fläche.'),
        HerbLayer(code=4, description='Die Krautschicht bedeckt die ganze Fläche (4/4).'),
    ])
    Landslide.objects.bulk_create([
        Landslide(code=0, description='Keine'),
        Landslide(code=1, description='Aktive Rutschung durch Vernässung.'),
        Landslide(code=2, description='Aktive Rutschung durch erodierenden Wasserlauf.'),
        Landslide(code=3, description='Alte Rutschung durch Vernässung.'),
        Landslide(code=4, description='Alte Rutschung durch erodierenden Wasserlauf.'),
    ])
    MecDamage.objects.bulk_create([
        MecDamage(code=0, description='keine Bäume des Hauptbestandes weisen Schäden auf.'),
        MecDamage(code=1, description='gerig, weniger als 1/3 aller Bäume des Hauptbestandes weisen Schäden auf.'),
        MecDamage(code=2, description='häufig, mehr als 1/3 aller Bäume des Hauptbestandes weisen Schäden auf.'),
        # https://gitlab.com/guaraci/ksp-zg/-/issues/2
        MecDamage(code=3, description='?'),
        MecDamage(code=4, description='?'),
        MecDamage(code=5, description='?'),
    ])
    MarkerType.objects.bulk_create([
        MarkerType(code='$', description='Markstein'),
        MarkerType(code='#', description='Schacht oder ähnliches'),
        MarkerType(code='"', description='Leitungsstangen oder Masten'),
        MarkerType(code='@', description='?Unbekannt?'),
    ])
    TreeSpecies.objects.bulk_create([
        TreeSpecies(code='B', description='Buche'),
        TreeSpecies(code='E', description='Eiche (alle)', historic=True),
        TreeSpecies(code='E', description='Stieleiche'),
        TreeSpecies(code='ET', description='Traubeneiche'),
        TreeSpecies(code='ER', description='Roteiche'),
        TreeSpecies(code='S', description='Esche'),
        TreeSpecies(code='A', description='Ahorn (alle)', historic=True),
        TreeSpecies(code='A', description='Bergahorn'),
        TreeSpecies(code='AS', description='Spitzahorn'),
        TreeSpecies(code='AF', description='Feldahorn'),
        TreeSpecies(code='M', description='Ulme (alle)'),
        TreeSpecies(code='X', description='Linde (beide)'),
        TreeSpecies(code='C', description="S'Erle"),
        TreeSpecies(code='V', description='Birke (beide)'),
        TreeSpecies(code='K', description='Kirschbaum', historic=True),
        TreeSpecies(code='K', description='Waldkirschbaum'),
        TreeSpecies(code='KT', description='Traubenkirsche'),
        TreeSpecies(code='U', description='Übriges Laubholz'),
        TreeSpecies(code='UH', description='Hagebuche'),
        TreeSpecies(code='UW', description='Weisserle'),
        TreeSpecies(code='UL', description='Pappel'),
        TreeSpecies(code='UP', description='Aspe'),
        TreeSpecies(code='UO', description='Vogelbeere'),
        TreeSpecies(code='UY', description='Mehlbeere'),
        TreeSpecies(code='UQ', description='Nussbaum'),
        TreeSpecies(code='UD', description='Weiden'),
        TreeSpecies(code='UR', description='Robinie'),
        TreeSpecies(code='UT', description='Kastanie'),
        TreeSpecies(code='UZ', description='Wildobst'),
        TreeSpecies(code='R', description='Rottanne'),
        TreeSpecies(code='T', description='Weisstanne'),
        TreeSpecies(code='F', description='Föhre'),
        TreeSpecies(code='L', description='Lärche'),
        TreeSpecies(code='D', description='Douglasie'),
        TreeSpecies(code='W', description='Weymouthsföhre'),
        TreeSpecies(code='N', description='Übriges Nadelholz'),
        TreeSpecies(code='NE', description='Eibe'),
    ])
    Vita.objects.bulk_create([
        Vita(code='D', description='Dürrständer'),
        Vita(code='L', description='Lebend'),
        Vita(code='E', description='Einwuchs'),
        Vita(code='N', description='Nutzung'),
        Vita(code='V', description='vergessener Baum'),
    ])


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(fill_tables),
    ]
