from django.contrib.gis.db.models.fields import MultiPolygonField
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0007_municipality_multipoly'),
    ]

    operations = [
        migrations.AddField(
            model_name='owner',
            name='geom',
            field=MultiPolygonField(blank=True, null=True, srid=2056),
        ),
        migrations.AddField(
            model_name='owner',
            name='id_spa',
            field=models.PositiveSmallIntegerField(blank=True, null=True),
        ),
    ]
