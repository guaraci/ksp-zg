import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0006_init_functions'),
    ]

    operations = [
        migrations.RunSQL(
            "ALTER TABLE municipality ALTER COLUMN the_geom type geometry(MultiPolygon, 2056) using ST_Multi(the_geom);",
            state_operations=[
                migrations.AlterField(
                    model_name='municipality',
                    name='the_geom',
                    field=django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=2056),
                ),
            ],
        )
    ]
