from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0018_change_user_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='plotobs',
            name='created',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Erstellt am'),
        ),
        migrations.AddField(
            model_name='plotobs',
            name='operator',
            field=models.ForeignKey(
                null=True, on_delete=models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL, verbose_name='Operator'
            ),
        ),
    ]
