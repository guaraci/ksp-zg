import django.contrib.auth.models
import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0012_alter_user_first_name_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(error_messages={'unique': 'A user with that username already exists.'}, help_text='Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.', max_length=150, unique=True, validators=[django.contrib.auth.validators.UnicodeUsernameValidator()], verbose_name='username')),
                ('first_name', models.CharField(blank=True, max_length=150, verbose_name='first name')),
                ('last_name', models.CharField(blank=True, max_length=150, verbose_name='last name')),
                ('email', models.EmailField(blank=True, max_length=254, verbose_name='email address')),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'db_table': 'auth_user',
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Municipality',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('name', models.CharField(max_length=50, unique=True)),
                ('bfs_nr', models.IntegerField(null=True)),
                ('plz', models.CharField(blank=True, max_length=4)),
                ('kanton', models.CharField(max_length=2)),
                ('the_geom', django.contrib.gis.db.models.fields.PolygonField(blank=True, null=True, srid=2056)),
            ],
            options={
                'db_table': 'municipality',
            },
        ),
        migrations.CreateModel(
            name='PlotAbsence',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=40, verbose_name='Beschreibung')),
            ],
            options={
                'db_table': 'lt_plot_absence',
            },
        ),
        migrations.CreateModel(
            name='Plot',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(max_length=15, verbose_name='KSP Nummer')),
                ('center', django.contrib.gis.db.models.fields.PointField(srid=2056, verbose_name='KSP Zentrum')),
                ('slope', models.PositiveSmallIntegerField(default=0, verbose_name='Neigung')),
                ('sealevel', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Höhe ü. Meer')),
                ('offset_dist', models.PositiveSmallIntegerField(default=0, verbose_name='Verschiebung Distanz')),
                ('offset_azi', models.PositiveSmallIntegerField(default=0, verbose_name='Verschiebung Azimuth')),
                ('mirror_dist', models.PositiveSmallIntegerField(default=0, verbose_name='Spiegelung Distanz')),
                ('mirror_azi', models.PositiveSmallIntegerField(default=0, verbose_name='Spiegelung Azimuth')),
                ('absent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.plotabsence', verbose_name='Fehlend')),
                ('remarks', models.TextField(blank=True, verbose_name='Bemerkungen')),
            ],
            options={
                'db_table': 'plot',
            },
        ),
        migrations.CreateModel(
            name='PlotObsStatus',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.PositiveSmallIntegerField()),
                ('description', models.CharField(max_length=30, verbose_name='Beschreibung')),
            ],
            options={
                'db_table': 'lt_plot_obs_status',
            },
        ),
        migrations.CreateModel(
            name='Mutation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1, unique=True)),
                ('short_descr', models.CharField(max_length=30, verbose_name='Kurzbeschreibung')),
                ('long_descr', models.CharField(max_length=100, verbose_name='Langbeschreibung')),
            ],
            options={
                'db_table': 'lt_mutation',
            },
        ),
        migrations.CreateModel(
            name='CrownClosure',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.PositiveSmallIntegerField()),
                ('description', models.CharField(max_length=80, verbose_name='Beschreibung')),
            ],
            options={
                'db_table': 'lt_crown_closure',
            },
        ),
        migrations.CreateModel(
            name='DevelStage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.PositiveSmallIntegerField()),
                ('description', models.CharField(max_length=80, verbose_name='Beschreibung')),
            ],
            options={
                'db_table': 'lt_devel_stage',
            },
        ),
        migrations.CreateModel(
            name='ForestMixture',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.PositiveSmallIntegerField()),
                ('description', models.CharField(max_length=80, verbose_name='Beschreibung')),
            ],
            options={
                'db_table': 'lt_forest_mixture',
            },
        ),
        migrations.CreateModel(
            name='Division',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1, unique=True)),
                ('description', models.CharField(max_length=30, verbose_name='Beschreibung')),
            ],
            options={
                'db_table': 'division',
            },
        ),
        migrations.CreateModel(
            name='OwnerCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=20, verbose_name='Kategorie')),
            ],
            options={
                'db_table': 'lt_owner_category',
            },
        ),
        migrations.CreateModel(
            name='OwnerType',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1, unique=True)),
                ('description', models.CharField(max_length=30, verbose_name='Beschreibung')),
                ('categ', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.ownercategory', verbose_name='Kategorie')),
            ],
            options={
                'db_table': 'ownertype',
            },
        ),
        migrations.CreateModel(
            name='Owner',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=80, verbose_name='Beschreibung')),
                ('typ', models.ForeignKey(db_column='type_id', on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.ownertype', verbose_name='Eigentümertyp')),
            ],
            options={
                'db_table': 'owner',
            },
        ),
        migrations.CreateModel(
            name='HerbLayer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.PositiveSmallIntegerField()),
                ('description', models.CharField(max_length=80, verbose_name='Beschreibung')),
            ],
            options={
                'db_table': 'lt_herblayer',
            },
        ),
        migrations.CreateModel(
            name='Landslide',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.PositiveSmallIntegerField()),
                ('description', models.CharField(max_length=80, verbose_name='Beschreibung')),
            ],
            options={
                'db_table': 'lt_landslide',
            },
        ),
        migrations.CreateModel(
            name='MecDamage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.PositiveSmallIntegerField()),
                ('description', models.CharField(max_length=80, verbose_name='Beschreibung')),
            ],
            options={
                'db_table': 'lt_mecdamage',
            },
        ),
        migrations.CreateModel(
            name='PlotObs',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('year', models.PositiveSmallIntegerField(db_index=True, verbose_name='Aufnahmejahr')),
                ('quarter', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Aufnahmequartal')),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ksp_zg.plot', verbose_name='Aufnahmepunkt (plot)')),
                ('status', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.plotobsstatus', verbose_name='Status')),
                ('mutation', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.mutation', verbose_name='Mutation')),
                ('remarks_status', models.TextField(blank=True, verbose_name='Bemerkungen zum Status')),
                ('devel_stage', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.develstage', verbose_name='Entwicklungstufe')),
                ('forest_mixture', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.forestmixture', verbose_name='Mischungsgrad')),
                ('crown_closure', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.crownclosure', verbose_name='Schlussgrad')),
                ('devel_stage_map', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='ksp_zg.develstage', verbose_name='Entwicklungstufe (Karte)')),
                ('forest_mixture_map', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='ksp_zg.forestmixture', verbose_name='Mischungsgrad (Karte)')),
                ('crown_closure_map', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='ksp_zg.crownclosure', verbose_name='Schlussgrad (Karte)')),
                ('survey', models.CharField(blank=True, max_length=5, verbose_name='Erhebung')),
                ('municipality', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.municipality', verbose_name='Gemeinde')),
                ('division', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.division', verbose_name='Abteilung')),
                ('owner', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.owner', verbose_name='Eigentümer')),
                ('remarks_ecode', models.TextField(blank=True, verbose_name='Bemerkungen zum E-CODE')),
                ('stand_age', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Bestandalter')),
                ('dead_wood', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Totholz')),
                ('soil_cond', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Ungünstige Bodenverhältnisse')),
                ('herb_layer', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.herblayer', verbose_name='Krautschicht')),
                ('landslide', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.landslide', verbose_name='Rutschung')),
                ('mec_damage', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.mecdamage', verbose_name='Mechanische Schäden')),
                ('remarks', models.TextField(blank=True, verbose_name='Allgemeine Bemerkungen')),
            ],
            options={
                'db_table': 'plot_obs',
            },
        ),
        migrations.CreateModel(
            name='MarkerType',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1)),
                ('description', models.CharField(max_length=50)),
            ],
            options={
                'db_table': 'marker_type',
            },
        ),
        migrations.CreateModel(
            name='Marker',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(max_length=15, verbose_name='Nummer')),
                ('azimuth', models.PositiveSmallIntegerField(verbose_name='GON (400)')),
                ('distance', models.PositiveSmallIntegerField(verbose_name='in Dezimetern [dm]')),
                ('mtype', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.markertype', verbose_name='Typ')),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ksp_zg.plot')),
            ],
            options={
                'db_table': 'marker',
            },
        ),
        migrations.CreateModel(
            name='Tree',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(max_length=15, verbose_name='Nummer')),
                ('azimuth', models.PositiveSmallIntegerField(verbose_name='GON (400)')),
                ('distance', models.PositiveSmallIntegerField(verbose_name='in Dezimetern [dm]')),
                ('plot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ksp_zg.plot')),
                ('mirror', models.BooleanField(default=False, verbose_name='Spiegelbaum?')),
            ],
            options={
                'db_table': 'tree',
            },
        ),
        migrations.CreateModel(
            name='TreeSpecies',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=4)),
                ('description', models.CharField(max_length=100)),
                ('historic', models.BooleanField(default=False)),
            ],
            options={
                'db_table': 'lt_tree_spec',
            },
        ),
        migrations.CreateModel(
            name='Vita',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=1, unique=True)),
                ('description', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'lt_vita',
            },
        ),
        migrations.CreateModel(
            name='TreeObs',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('obs', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ksp_zg.plotobs')),
                ('tree', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ksp_zg.tree', verbose_name='Baum')),
                ('dbh', models.PositiveSmallIntegerField(verbose_name='Brusthöhendurchmesser in cm')),
                ('vita', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.vita', verbose_name='Lebenslauf')),
                ('remarks', models.TextField(blank=True, verbose_name='Bemerkungen')),
            ],
            options={
                'db_table': 'tree_obs',
            },
        ),
        migrations.AddField(
            model_name='tree',
            name='spec',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='ksp_zg.treespecies'),
        ),
        migrations.AddConstraint(
            model_name='plotobs',
            constraint=models.CheckConstraint(check=models.Q(('year__gte', 1970), ('year__lte', 2100)), name='year_range_check'),
        ),
        migrations.AddConstraint(
            model_name='plotobs',
            constraint=models.CheckConstraint(check=models.Q(('quarter__lte', 4)), name='quarter_max_4'),
        ),
        migrations.AddConstraint(
            model_name='treeobs',
            constraint=models.UniqueConstraint(fields=('obs', 'tree'), name='unique_tree_per_plot_obs'),
        ),
        migrations.AddConstraint(
            model_name='treespecies',
            constraint=models.UniqueConstraint(fields=('code', 'historic'), name='unique_code_non_historic'),
        ),
    ]
