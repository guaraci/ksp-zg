from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0010_rename_description_owner_name'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tree',
            old_name='nr',
            new_name='old_nr',
        ),
        migrations.AlterField(
            model_name='tree',
            name='old_nr',
            field=models.CharField(max_length=15, verbose_name='alte Nummer'),
        ),
        migrations.AddField(
            model_name='tree',
            name='nr',
            field=models.PositiveIntegerField(default=0, verbose_name='Baumnummer'),
            preserve_default=False,
        ),
    ]
