import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0008_owner_geom_and_id_spa'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdminRegion',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nr', models.CharField(max_length=10, unique=True)),
                ('name', models.CharField(max_length=100)),
                ('region_type', models.CharField(choices=[
                    ('frw', 'Forstrevier'), ('erhw', 'Erholungswald'), ('schw', 'Schutzwald'), ('nat', 'Naturschutzgebiet'), ('warm', 'Wärmegliederung'), ('frei', 'Freiperimeter')
                ], max_length=10)),
                ('geom', django.contrib.gis.db.models.fields.MultiPolygonField(blank=True, null=True, srid=2056)),
            ],
            options={
                'db_table': 'adminregion',
            },
        ),
    ]
