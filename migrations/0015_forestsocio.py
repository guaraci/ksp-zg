import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0014_incclass_phytosoc'),
    ]

    operations = [
        migrations.CreateModel(
            name='ForestSocio',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kartierer', models.CharField(max_length=50, verbose_name='Kartierer')),
                ('geom', django.contrib.gis.db.models.fields.PolygonField(srid=2056, verbose_name='Geometrie')),
                ('remarks', models.TextField(blank=True, verbose_name='Bemerkungen')),
                ('phytosoc', models.ForeignKey(blank=True, null=True, on_delete=models.deletion.PROTECT, to='ksp_zg.phytosoc')),
            ],
            options={
                'verbose_name': 'Waldgesellschaft',
                'verbose_name_plural': 'Waldgesellschaften',
                'db_table': 'waldgesellschaft',
            },
        ),
    ]
