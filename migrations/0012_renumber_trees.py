from django.db import migrations
from django.db.models import F


def renumber_trees(apps, schema_editor):
    Plot = apps.get_model('ksp_zg', 'Plot')
    Tree = apps.get_model('ksp_zg', 'Tree')

    Tree.objects.update(nr=0)
    for plot in Plot.objects.all().prefetch_related('plotobs_set'):
        for obs in plot.plotobs_set.all().prefetch_related('treeobs_set').order_by('inv_period'):
            new_nr = 1
            trees = Tree.objects.filter(treeobs__obs=obs).order_by('azimuth', 'distance')
            new_nr = max([t.nr for t in trees if t.nr is not None] + [0]) + 1
            for tree in trees:
                if not tree.nr:
                    tree.nr = new_nr
                    tree.save()
                    new_nr += 1


class Migration(migrations.Migration):

    dependencies = [
        ('ksp_zg', '0011_tree_old_nr_new_nr'),
    ]

    operations = [
        migrations.RunPython(renumber_trees, migrations.RunPython.noop)
    ]
