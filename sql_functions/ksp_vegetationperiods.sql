CREATE OR REPLACE FUNCTION public.ksp_vegetationperiods(
	year0 smallint, quarter0 smallint, year1 smallint, quarter1 smallint)
    RETURNS smallint
    LANGUAGE 'sql'
    COST 100
    IMMUTABLE PARALLEL SAFE
AS $BODY$
    SELECT CASE
        WHEN quarter0=1 AND quarter1 IN (2,3,4) THEN year1 - (year0 - 1)
        WHEN quarter0=1 AND quarter1=1 THEN (year1 - 1) - (year0 - 1)
        WHEN quarter0 IN (2,3,4) AND quarter1=1 THEN (year1 -1 ) - year0
        ELSE year1 - year0
    END
$BODY$;

COMMENT ON FUNCTION public.ksp_vegetationperiods(smallint, smallint, smallint, smallint)
    IS 'Funktion zur Herleitung der Anzahl Vegetationsperioden zwischen zwei Inventuren aufgrund der Jahre und der Quartale. Dabei gilt: Q1 --> Vegetationsperiode des Vorjahres';
