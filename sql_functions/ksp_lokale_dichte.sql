CREATE OR REPLACE FUNCTION public.ksp_lokale_dichte(edgef numeric)
    RETURNS numeric
    LANGUAGE 'sql'
    COST 100
    IMMUTABLE PARALLEL SAFE
AS $BODY$
    SELECT 100.0 / 3.14;
$BODY$;

COMMENT ON FUNCTION public.ksp_lokale_dichte(numeric)
    IS 'Repräsentationsfläche eines Aufnahmeplots in Abhängigkeit des Waldrandfaktors. Die lokale Dichte bezeichnet die am Stichprobenpunkt festgestellte (gemessene) räumlich Dichte der Zielvariable pro Flächeneinheit (normalerweise pro Hektar).';
