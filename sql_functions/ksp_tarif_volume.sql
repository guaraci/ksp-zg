CREATE OR REPLACE FUNCTION public.ksp_tarif_volume(diameter smallint, tarif_klass character varying)
    RETURNS numeric
    LANGUAGE 'sql'
    COST 100
    IMMUTABLE PARALLEL SAFE 
AS $BODY$
  SELECT CASE
    WHEN diameter <= 0 THEN 0
    WHEN tarif_klass IN ('buche', 'ubrige_laub') THEN exp(-3.11115+3.013630*ln(diameter)-0.00350483*ln(diameter)^4)/1000
    WHEN  tarif_klass IN ('fichte','tanne','ubrige_nad') THEN exp(-3.71010+3.248818*ln(diameter)-0.00440613*ln(diameter)^4)/1000
    ELSE 0
  END
$BODY$;

COMMENT ON FUNCTION public.ksp_tarif_volume(smallint, character varying)
    IS 'Modell gemäss Tarif Kanton Zug 1990. Differenzierung nach Laubholz/Nadelholz des. Funktion zur Berechnung  Schaftholzvolumen in Rinde der Probebäum';
