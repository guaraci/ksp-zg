CREATE OR REPLACE FUNCTION public.ksp_grundflaeche_bl(diameter smallint)
    RETURNS double precision
    LANGUAGE 'sql'
    COST 100
    IMMUTABLE PARALLEL SAFE 
AS $BODY$
  SELECT pi() * $1 * $1 / 4 / 10000;
$BODY$;

COMMENT ON FUNCTION public.ksp_grundflaeche_bl(smallint)
    IS 'Funktion zur Berechnung der Stammquerschnittsfläche eines Probebaums aufgrund des BHD (BHD-Messstelle in 1.3m Höhe). Ausgabe in m2.';
