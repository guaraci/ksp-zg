# App KSP-ZG

KSP-ZG is an application meant to be integrated in a [KSP](https://gitlab.com/guaraci/ksp)
parent [Django](https://www.djangoproject.com) project, adding the specificity
of forest inventories for the Zug canton (data from 1972).

## Installation

1. Install first a working KSP project.

2. Clone this Git repository inside the KSP project and add `ksp_zg` app to the
   project's INSTALLED_APPS.
