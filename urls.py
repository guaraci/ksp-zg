from django.urls import path

from . import views

urlpatterns = [
    path('', views.ZGGemeindenView.as_view(), name='home'),
    path('gemeinde/<int:pk>/', views.GemeindeDetailView.as_view(), name='gemeinde'),
    path('plotobs/', views.PlotObsDataView.as_view(), name='plotobs'),
]
