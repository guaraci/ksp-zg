from django.db.models import Avg, F, OuterRef, Subquery

from django.urls import reverse

from observation.models.db_views import (
    Einwuchs, EinwuchsProSpec, #EinwuchsProSpecGroup,
    BaseHolzProduktion, HolzProduktion, HolzProduktionProSpec, #HolzProduktionProSpecGroup,
    Nutzung, NutzungProSpec, #NutzungProSpecGroup,
    Totholz, TotholzProSpec, #TotholzProSpecGroup,
)
from observation.queries import (
    Groupment, GroupmentRegion as GroupmentRegionBase, GroupmentSpec,
    GemeindePerimGroup, HeutOwnerPerimGroup, HistOwnerPerimGroup,
    PerimeterSetBase, PerimGroup, QueryData, ZuwachsQuery
)
from .models import AdminRegion, Municipality, Owner, PlotObs


class HolzProduktion16(BaseHolzProduktion):
    class Meta:
        db_table = "bv_grundf_vol_yx_pro_plotobs_dbh16"
        managed = False


VIEW_MAP = {
    'Stammzahl12': QueryData(
        model=HolzProduktion, descr='Stammzahl, Volumen, Grundfläche (BHD >11)',
        model_by_spec=HolzProduktionProSpec, model_by_spec_group=None,#HolzProduktionProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Holzproduktion',
        help_id='Stammzahl',
    ),
    'Stammzahl16': QueryData(
        model=HolzProduktion16, descr='Stammzahl, Volumen, Grundfläche (BHD >15)',
        #model_by_spec=HolzProduktionProSpec, model_by_spec_group=None,#HolzProduktionProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Holzproduktion',
        help_id='Stammzahl',
    ),
    'Totholz': QueryData(
        model=Totholz, descr='Totholz',
        model_by_spec=TotholzProSpec, model_by_spec_group=None,#TotholzProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Naturschutz',
    ),
    'Einwuchs': QueryData(
        model=Einwuchs, descr='Einwuchs',
        model_by_spec=EinwuchsProSpec, model_by_spec_group=None,#EinwuchsProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Holzproduktion',
    ),
    'Nutzung': QueryData(
        model=Nutzung, descr='Nutzung',
        model_by_spec=NutzungProSpec, model_by_spec_group=None,#NutzungProSpecGroup,
        plot_obs='plotobs_id__', order_by='plotobs',
        map_color_fields=['anzahl_baume_rel'],
        annot_map={'anzahl_baume_rel': Avg, 'volumen_rel': Avg, 'grundflaeche_rel': Avg},
        groupables=['spec', 'group', 'otype', 'ostatus'],
        category='Holzproduktion',
    ),
    'Zuwachs': ZuwachsQuery(),
    # Essentially to query verbose field names for some groupable keys not on views
    'plotobs': QueryData(model=PlotObs, category=''),
}


class SpecialPerimGroup(PerimGroup):
    """Mixed AdminRegions in the same group"""
    adminr_keys = ['erhw', 'nat', 'schw']

    @property
    def is_adminregion(self):
        return True

    def pks_from_request(self, qdict):
        pks = []
        for key in self.adminr_keys:
            pks.extend([int(v) for v in qdict.getlist(key)])
        return pks

    def item_name(self, item):
        if item.region_type == 'nat':
            return f'{item.name} (Naturschutz)'
        if item.region_type == 'schw':
            return f'{item.name} (Schutzwald)'
        return item.name

    def __iter__(self):
        for item in self.items:
            yield {
                'name': self.item_name(item), 'pk': item.pk,
                'input_id': f'{item.region_type}{item.pk}', 'input_name': item.region_type
            }


class PerimeterSet(PerimeterSetBase):
    @classmethod
    def get_perimeters(cls):
        kanton = AdminRegion.objects.filter(region_type='frei', nr='kant').first()
        return [
            GemeindePerimGroup(
                "Gemeinden",
                Municipality.objects.all().order_by('name'),
                _infos="Politische Gemeinden des Kantons Zug",
                infos_url=reverse('doc-specific', args=['gemeinden']),
                short_name="gem",
            ),
            PerimGroup(
                "Forstrevier",
                AdminRegion.objects.filter(region_type='frw').order_by('name'),
                _infos="Alle Forstreviere des Kantons Zug können ausgewählt werden.",
                short_name="frevier",
            ),
            HistOwnerPerimGroup(
                _infos=("Wichtigste Waldeigentümer des Kantons Zug können ausgewählt werden. "
                        "(Eigentümer zum Zeitpunkt der Inventarisierung)"),
            ),
            HeutOwnerPerimGroup(
                _items=Owner.objects.exclude(geom__isnull=True).order_by('name'),
                _infos=("Wichtigste Waldeigentümer des Kantons Zug können ausgewählt werden. "
                        "(Aktueller Stand der Eigentumsverhältnisse)"),
            ),
            PerimGroup(
                "Wärmegliederung",
                AdminRegion.objects.filter(region_type='warm').order_by('name'),
                short_name="warm",
            ),
            PerimGroup(
                "Freiperimeter",
                AdminRegion.objects.filter(region_type='frei').exclude(nr='kant').order_by('name'),
                short_name="frei",
            ),
            SpecialPerimGroup(
                "Weitere Perimeter",
                (
                    ([kanton] if kanton is not None else []) +
                    list(AdminRegion.objects.filter(region_type='erhw').order_by('name')) +
                    list(AdminRegion.objects.filter(region_type='nat').order_by('name')) +
                    list(AdminRegion.objects.filter(region_type='schw').order_by('name'))
                ),
            ),
        ]


class GroupmentRegion(GroupmentRegionBase):
    @property
    def regiontype_name(self):
        return {
            'forstrevier': 'frw',
        }.get(self.code, self.code)


class GroupmentOwner(Groupment):
    def apply_to(self, query_data, query):
        # join plot/owner by plot center inside Owner geom (through subquery).
        owner_sq = Owner.objects.filter(geom__contains=OuterRef('plot__center'))
        query = query.annotate(ownername=Subquery(owner_sq.values('name')[:1]))
        aggr_crit = 'ownername'
        field_name = f'*{self.label}'
        return aggr_crit, field_name, query


def get_groupments():
    return {
        'perims': [
            Groupment('municipality', 'Gemeinde'),
            GroupmentRegion('forstrevier', 'Forstrevier'),
            Groupment(
                'eigentumer-name', 'Hist. Eigentümer', accessor='owner__name',
                docs_title="Gruppierung nach wichtigsten Waldeigentümern (zum Zeitpunkt der Inventarisierung)"
            ),
            GroupmentOwner(
                'eigentumer-geom', 'Heut. Eigentümer',
                docs_title="Gruppierung nach wichtigsten Waldeigentümern (Aktueller Stand der Eigentumsverhältnisse)",
            ),
            GroupmentRegion('erhw', 'Erholungswald'),
            GroupmentRegion('schw', 'Schutzwald'),
            GroupmentRegion('nat', 'Naturschutzgebiet'),
            GroupmentRegion('warm', 'Wärmegliederung'),
            GroupmentRegion('frei', 'Freiperimeter'),
        ],
        'periods': [
            Groupment('year', 'Jahr'),
            Groupment(
                'inv_period', 'Aufnahmeperiode', accessor='inv_team__period', checked=True,
                docs_title="Erste Phase (1972-1974), Zweite Phase (1991-1993), Dritte Phase (2008-2010)",
            ),
        ],
        'bestand': [
            Groupment(
                'devel_stage', "Entwicklungsstufe",
                docs_title="Bestimmte Etappe der Entwicklung eines Waldbestandes charakterisiert durch dessen Grösse.",
                docs_url=reverse('doc-specific', args=['entwicklungsstufe'])),
            Groupment(
                'forest_mixture', "Mischungsgrad",
                docs_title="In einem Bestand oder in einer Bestandesschicht vorhandene Baumartenmischung (Nadel-/Laubholz).",
                docs_url=reverse('doc-specific', args=['mischungsgrad'])),
            Groupment(
                'crown_closure', "Schlussgrad",
                docs_title="Mass für die Intensität der Kronenberührungen innerhalb eines Bestandes.",
                docs_url=reverse('doc-specific', args=['schlussgrad'])),
        ],
        'phytosoc': [],
        'various': [
            GroupmentSpec('spec', "Baumart"),
            #GroupmentSpecGroup('group', "Baumartgruppe"),
            Groupment(
                'otype', "Eigentümertyp", accessor='owner__typ', fkey_target='__description',
                docs_title="Die Waldeigentümer werden in verschiedene Kategorien (Tabelle owner_type) gruppiert. Damit kann die Auswertung pro Eigentümertyp umgesetzt werden (Waldentwicklungsplanung). Die Erläuterungen zum Eigentümer sind unter Eigentümer aufgeführt."
            ),
            Groupment(
                'ostatus', "Eigentümer Status", accessor='owner__typ__categ', fkey_target='__description',                docs_title="Die Waldeigentümer werden in die Kategorien i) öffentlich (Korporationen, Einwohnergemeinden, "
                           "Bürgergemeinden, Kirchgemeinden, Staatswald, Bund) und ii) privat (Privatwaldbesitzer, Klöster, ""Waldgenossenschaften) eingeteilt."),
        ],
    }
